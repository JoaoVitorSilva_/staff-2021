package br.com.dbccompany.coworkingLogs.Repository;

import br.com.dbccompany.coworkingLogs.Entity.LogEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends MongoRepository<LogEntity, String> {
    List<LogEntity> findByCodigo(String codigo);
    List<LogEntity> findByMensagem(String mensagem);
    List<LogEntity> findByCodigoAndMensagem(String codigo, String mensagem);
}
