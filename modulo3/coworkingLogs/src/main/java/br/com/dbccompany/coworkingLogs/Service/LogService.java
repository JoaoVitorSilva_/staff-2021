package br.com.dbccompany.coworkingLogs.Service;

import br.com.dbccompany.coworkingLogs.CoworkingLogsApplication;
import br.com.dbccompany.coworkingLogs.DTO.LogDTO;
import br.com.dbccompany.coworkingLogs.Entity.LogEntity;
import br.com.dbccompany.coworkingLogs.Repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {
    @Autowired
    LogRepository logsRepository;

    private Logger logger = LoggerFactory.getLogger(CoworkingLogsApplication.class);

    @Transactional
    public List<LogDTO> todos() {
        try {
            List<LogDTO> logsDTOS = new ArrayList<>();
            for (LogEntity logEntity: logsRepository.findAll()) {
                logsDTOS.add(new LogDTO(logEntity));
            }
            return logsDTOS;
        } catch (Exception e) {
            logger.error("Erro ao listar logs: " + e.getMessage());
            throw new RuntimeException();
        }

    }

    @Transactional
    public LogDTO salvarLog(LogDTO logDTO) {
        try {
            LogEntity logFinal = logDTO.convert();
            LogDTO newDTO = new LogDTO(logsRepository.save(logFinal));
            return newDTO;
        } catch (Exception e) {
            logger.error("Erro ao salvar log: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public LogDTO buscarPorId(String id) {
        try {
            return new LogDTO(logsRepository.findById(id).get());
        } catch (Exception e) {
            logger.error("Log de id: " + id +" não foi encontrado. Mensagem: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
