package br.com.dbccompany.coworkingLogs.DTO;

import br.com.dbccompany.coworkingLogs.Entity.LogEntity;

public class LogDTO {

    private String id;
    private String codigo;
    private String mensagem;

    public LogDTO() {}

    public LogDTO(LogEntity logAplicacao) {
        this.id = logAplicacao.getId();
        this.codigo = logAplicacao.getCodigo();
        this.mensagem = logAplicacao.getMensagem();
    }

    @Override
    public String toString() {
        return String.format("Log[id=%s, cogigo='%s', mensagem='%s']", id, codigo, mensagem);
    }

    public LogEntity convert() {
        LogEntity logAplicacaoEntity = new LogEntity();
        logAplicacaoEntity.setId(this.id);
        logAplicacaoEntity.setCodigo(this.codigo);
        logAplicacaoEntity.setMensagem(this.mensagem);
        return logAplicacaoEntity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

}
