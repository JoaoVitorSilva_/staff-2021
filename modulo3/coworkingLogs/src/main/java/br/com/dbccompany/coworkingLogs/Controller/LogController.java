package br.com.dbccompany.coworkingLogs.Controller;

import br.com.dbccompany.coworkingLogs.CoworkingLogsApplication;
import br.com.dbccompany.coworkingLogs.DTO.LogDTO;
import br.com.dbccompany.coworkingLogs.Service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/apiLogs")
public class LogController {

    @Autowired
    LogService logsService;

    private Logger logger = LoggerFactory.getLogger(CoworkingLogsApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<LogDTO> todosLogs() {
        logger.info("Buscar os logs da aplicação.");
        logger.warn("Não havendo cadastros poderá retornar vazia!");
        return logsService.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public LogDTO salvar(@RequestBody LogDTO logsDTO){
        logger.info("Adicionando novo log na aplicação.");
        return logsService.salvarLog(logsDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public LogDTO espacoEspecifico(@PathVariable String id) {
        logger.info("Buscando o log de id: " + id);
        return logsService.buscarPorId(id);
    }
}
