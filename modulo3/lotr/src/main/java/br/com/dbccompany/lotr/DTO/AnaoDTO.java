package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;

public class AnaoDTO {
    private String nome;
    private Double vida;
    private StatusEnum status;
    private Double qtdDano = 0.0;
    private int experiencia = 0;
    private float qtdExperienciaPorAtaque = 1;
    private InventarioEntity inventario;

    public AnaoDTO(AnaoEntity anao) {
        this.nome = anao.getNome();
        this.vida = anao.getVida();
        this.status = anao.getStatus();
        this.qtdDano = anao.getQtdDano();
        this.experiencia = anao.getExperiencia();
        this.qtdExperienciaPorAtaque = anao.getQtdExperienciaPorAtaque();
        this.inventario = anao.getInventario();
    }

    public AnaoDTO(){

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public float getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(float qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
