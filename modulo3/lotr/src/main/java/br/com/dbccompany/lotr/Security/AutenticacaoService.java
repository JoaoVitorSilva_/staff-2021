package br.com.dbccompany.lotr.Security;

import br.com.dbccompany.lotr.Entity.UsuarioEntity;
import br.com.dbccompany.lotr.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AutenticacaoService implements UserDetailsService {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<UsuarioEntity> usuario = this.repository.findByLogin(login);
        if (usuario.isPresent()) {
            return (UserDetails) new UsuarioEntity(usuario.get());
        }
        throw new UsernameNotFoundException("Usuário inexistente!");
    }

    public List<UsuarioEntity> trazerTodos() {
        return (List<UsuarioEntity>) this.repository.findAll();
    }

    public UsuarioEntity encontrarPorLogin(String login) {
        return this.repository.findByLogin(login).orElse(null);
    }

    public UsuarioEntity salvarUsuario(UsuarioEntity usuario) {
        return this.repository.save(usuario);
    }

}
