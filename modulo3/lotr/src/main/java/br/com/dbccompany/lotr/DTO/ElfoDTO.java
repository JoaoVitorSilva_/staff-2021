package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;

import javax.persistence.*;

public class ElfoDTO {
    private String nome;
    private Double vida;
    private StatusEnum status;
    private Double qtdDano = 0.0;
    private int experiencia = 0;
    private float qtdExperienciaPorAtaque = 1;
    private InventarioEntity inventario;

    public ElfoDTO(ElfoEntity elfo) {
        this.nome = elfo.getNome();
        this.vida = elfo.getVida();
        this.status = elfo.getStatus();
        this.qtdDano = elfo.getQtdDano();
        this.experiencia = elfo.getExperiencia();
        this.qtdExperienciaPorAtaque = elfo.getQtdExperienciaPorAtaque();
        this.inventario = elfo.getInventario();
    }

    public ElfoDTO(){

    }

    public ElfoEntity converter(){
        ElfoEntity elfo = new ElfoEntity(this.nome);
        elfo.setVida(this.vida);
        elfo.setStatus(this.status);
        elfo.setQtdDano(this.qtdDano);
        elfo.setExperiencia(this.experiencia);
        elfo.setQtdExperienciaPorAtaque(this.qtdExperienciaPorAtaque);
        elfo.setInventario(this.inventario);
        return elfo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public float getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(float qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
