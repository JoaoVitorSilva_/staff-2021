package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ElfoService{
    @Autowired
    ElfoRepository repository;

    private List<ElfoDTO> converterLista(List<ElfoEntity> elfoList){
        List<ElfoDTO> novaLista = new ArrayList<>();
        for(ElfoEntity elfo: elfoList){
            novaLista.add(new ElfoDTO(elfo));
        }
        return novaLista;
    }

    public List<ElfoDTO> trazerTodosPersonagens(){
        return converterLista((List<ElfoEntity>) repository.findAll());
    }

    @Transactional(rollbackFor = Exception.class )
    public ElfoDTO salvar(ElfoEntity elfo){
        return new ElfoDTO(this.salvarEEditar(elfo));
    }

    @Transactional(rollbackFor = Exception.class )
    public ElfoDTO editar(ElfoEntity elfo, Integer id){
        elfo.setId(id);
        return new ElfoDTO(this.salvarEEditar(elfo));
    }

    public ElfoEntity salvarEEditar (ElfoEntity elfo){
        return repository.save(elfo);
    }

    public ElfoDTO buscarPorId(Integer id){
        Optional<ElfoEntity> elfo = repository.findById(id);
        return elfo.isPresent() ? new ElfoDTO(elfo.get()) : null;
    }

    public List<ElfoDTO> buscarTodosPorId(Integer id){
        return this.converterLista(repository.findAllById(id));
    }

    public List<ElfoDTO> buscarTodosPorIdIn(List<Integer> ids){
        return this.converterLista(repository.findAllByIdIn(ids));
    }

    public ElfoDTO buscarPorNome(String nome){
        return new ElfoDTO(repository.findByNome(nome));
    }

    public List<ElfoDTO> buscarTodosPorNome(String nome){
        return this.converterLista(repository.findAllByNome(nome));
    }

    public List<ElfoDTO> buscarTodosPorNomeIn(List<String> nomeList){
        return this.converterLista(repository.findAllByNomeIn(nomeList));
    }

    public ElfoDTO buscarPorExperiencia (Integer experiencia){
        return new ElfoDTO(repository.findByExperiencia(experiencia));
    }

    public List<ElfoDTO> buscarTodosPorExperiencia (Integer experiencia){
        return this.converterLista(repository.findAllByExperiencia(experiencia));
    }

    public List<ElfoDTO> buscarTodosPorExperienciaIn (List<Integer> experienciaList){
        return this.converterLista(repository.findAllByExperienciaIn(experienciaList));
    }

    public ElfoDTO buscarPorVida( Double vida ){
        return new ElfoDTO(repository.findByVida(vida));
    }

    public List<ElfoDTO> buscarTodosPorVida ( Double vida){
        return this.converterLista(repository.findAllByVida(vida));
    }

    public List<ElfoDTO> buscarTodosPorVidaIn (List<Double> vidaList){
        return this.converterLista(repository.findAllByVidaIn(vidaList));
    }

    public ElfoDTO buscarPorQtdDano ( Double dano){
        return new ElfoDTO(repository.findByQtdDano(dano));
    }

    public List<ElfoDTO> buscarTodosPorQtdDano ( Double dano){
        return this.converterLista(repository.findAllByQtdDano(dano));
    }

    public List<ElfoDTO> buscarTodosPorQtdDanoIn ( List<Double> danoList){
        return this.converterLista(repository.findAllByQtdDanoIn(danoList));
    }

    public ElfoDTO buscarPorQtdExperienciaPorAtaque( Integer expPorAtk ){
        return new ElfoDTO(repository.findByQtdExperienciaPorAtaque(expPorAtk));
    }

    public List<ElfoDTO> buscarTodosPorQtdExperienciaPorAtaque ( Integer expPorAtk){
        return this.converterLista(repository.findAllByQtdExperienciaPorAtaque(expPorAtk));
    }

    public List<ElfoDTO> buscarTodosPorQtdExperienciaPorAtaqueIn ( List<Integer> expPorAtkList){
        return this.converterLista(repository.findAllByQtdExperienciaPorAtaqueIn(expPorAtkList));
    }

    public ElfoDTO buscarPorInventario(InventarioEntity inventario){
        return new ElfoDTO(repository.findByInventario(inventario));
    }

    public List<ElfoDTO> buscarTodosPorInventario(InventarioEntity inventario){
        return this.converterLista(repository.findAllByInventario(inventario));
    }

    public List<ElfoDTO> buscarTodosPorInventarioIn(List<InventarioEntity> inventarioList){
        return this.converterLista(repository.findAllByInventarioIn(inventarioList));
    }
}
