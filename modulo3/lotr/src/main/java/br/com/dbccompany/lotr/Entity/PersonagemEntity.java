package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue( value = "personagem" )
public abstract class PersonagemEntity {

    @Id
    @SequenceGenerator( name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE )
    protected Integer id;

    @Column( unique = true, nullable = false )
    protected String nome;

    @Column( nullable = false, precision = 4, scale = 2 )
    protected Double vida;

    @Enumerated( EnumType.STRING )
    protected StatusEnum status;

    @Column( nullable = false, precision = 4, scale = 2, columnDefinition = "Double default 0")
    protected Double qtdDano = 0.0;

    @Column(nullable = false)
    protected int experiencia = 0;

    @Column(nullable = false)
    protected float qtdExperienciaPorAtaque = 1;

    public PersonagemEntity(String nome) {
        this.nome = nome;
    }

    @OneToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_inventario")
    private InventarioEntity inventario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public float getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(float qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
