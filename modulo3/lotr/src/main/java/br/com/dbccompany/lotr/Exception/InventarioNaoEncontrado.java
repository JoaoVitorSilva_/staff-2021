package br.com.dbccompany.lotr.Exception;

public class InventarioNaoEncontrado extends InventarioException {

    public InventarioNaoEncontrado() {
        super("Esse inventário não existe!");
    }
}
