package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping( value = "/api/item" )
public class ItemController {

    @Autowired
    private ItemService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<ItemDTO> trazerTodosItens(){
        return service.trazerTodosOsItens();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ItemDTO trazerItemEspecifico( @PathVariable Integer id ){
        return service.buscarPorId(id);
    }

    /*@PostMapping( value = "/salvar" )
    @ResponseBody
    public ItemEntity salvarItem( @RequestBody ItemEntity item ){
        return service.salvar(item);
    }*/

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ItemDTO salvarItem( @RequestBody ItemDTO item ){
        return service.salvar(item.converter());
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ItemDTO editarItem( @RequestBody ItemDTO item, @PathVariable Integer id){
        return service.editar(item.converter(), id);
    }

    @GetMapping( value = "/buscar/descricao/{ descricao }" )
    @ResponseBody
    public ItemDTO buscarPorDescricao(@PathVariable String descricao){
        return service.buscarPorDescricao(descricao);
    }

    @GetMapping( value = "/buscar/todos/descricao/{ descricao }" )
    @ResponseBody
    public List<ItemDTO> buscarTodosPorDescricao(@PathVariable String descricao){
        return service.buscarTodosPorDescricao(descricao);
    }

    @PostMapping( value = "/buscar/todosin/descricao" )
    @ResponseBody
    public List<ItemDTO> buscarTodosPorDescricaoIn(@RequestBody List<String> descricaoList){
        return service.buscarTodosPorDescricaoIn(descricaoList);
    }

    @GetMapping( value = "/buscar/todos/id/{ id }")
    @ResponseBody
    public List<ItemDTO> buscarTOdosPorId ( @PathVariable Integer id){
        return service.buscarTodosPorId(id);
    }

    @PostMapping( value = "/buscar/todosin/id" )
    @ResponseBody
    public List<ItemDTO> buscarTodosPorIdIn(@RequestBody List<Integer> idList){
        return service.buscarTodosPorIdIn(idList);
    }

    @PostMapping( value = "/buscar/inventarioItem" )
    @ResponseBody
    public ItemDTO buscarPorInventarioItem(@RequestBody Inventario_X_ItemDTO inventarioItem){
        return service.buscarPorInventarioItem(inventarioItem.converter());
    }

    @PostMapping( value = "/buscar/todos/inventarioItem" )
    @ResponseBody
    public List<ItemDTO> buscarTodosPorInventarioItem(@RequestBody Inventario_X_ItemDTO inventarioItem){
        return service.buscarTodosPorInventarioItem(inventarioItem.converter());
    }

    @PostMapping( value = "/buscar/todosIn/inventarioItem" )
    @ResponseBody
    public List<ItemDTO> buscarTodosPorInventarioItem(@RequestBody List<Inventario_X_ItemDTO> inventarioItem){
        List<Inventario_X_Item> novaLista = new ArrayList<>();
        for(Inventario_X_ItemDTO ixi : inventarioItem){
            novaLista.add(ixi.converter());
        }
        return service.buscarTodosPorInventarioItem(novaLista);
    }


}
