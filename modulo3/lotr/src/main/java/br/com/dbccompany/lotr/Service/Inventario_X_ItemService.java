package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Inventario_X_ItemService {

    @Autowired
    Inventario_X_ItemRepository repository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    InventarioRepository invRepository;

    @Transactional( rollbackFor = Exception.class)
    public Inventario_X_ItemDTO salvar( Inventario_X_Item inventarioItem ){
        return new Inventario_X_ItemDTO(this.salvarEEditar( inventarioItem ));
    }

    @Transactional( rollbackFor = Exception.class)
    public Inventario_X_ItemDTO editar( Inventario_X_Item inventarioItem, Inventario_X_ItemId id ){
        inventarioItem.setId(id);
        return new Inventario_X_ItemDTO(this.salvarEEditar( inventarioItem ));
    }

    private Inventario_X_Item salvarEEditar( Inventario_X_Item inventario_x_item ){
        ItemEntity item = itemRepository.findById(inventario_x_item.getId().getId_item()).get();
        InventarioEntity inventario = invRepository.findById(inventario_x_item.getId().getId_inventario()).get();
        inventario_x_item.setInventario(inventario);
        inventario_x_item.setItem(item);
        return repository.save( inventario_x_item );
    }

    private List<Inventario_X_ItemDTO> converterLista(List<Inventario_X_Item> inventarioItemList){
        List<Inventario_X_ItemDTO> novaLista = new ArrayList<>();
        for(Inventario_X_Item inventarioItem: inventarioItemList){
            novaLista.add(new Inventario_X_ItemDTO(inventarioItem));
        }
        return novaLista;
    }

    public List<Inventario_X_ItemDTO> trazerTodosInventario_X_Item(){
        return this.converterLista((List<Inventario_X_Item>) repository.findAll());
    }

    public Inventario_X_ItemDTO buscarPorId(Inventario_X_ItemId id){
        Optional<Inventario_X_Item> inventarioItem = repository.findById(id);
        if(inventarioItem.isPresent()){
            return new Inventario_X_ItemDTO( repository.findById(id).get() );
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorId(Inventario_X_ItemId id){
        return this.converterLista(repository.findAllById(id));
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorId(List<Inventario_X_ItemId> idList){
        return this.converterLista(repository.findAllByIdIn(idList));
    }

    public Inventario_X_ItemDTO buscarPorInventario(InventarioEntity inventario){
        return new Inventario_X_ItemDTO(repository.findByInventario(inventario));
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorInventario(InventarioEntity inventario){
        return this.converterLista(repository.findAllByInventario(inventario));
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorInventarioIn(List<InventarioEntity> inventarioList){
        return this.converterLista(repository.findAllByInventarioIn(inventarioList));
    }

    public Inventario_X_ItemDTO buscarPorItem(ItemEntity item){
        return new Inventario_X_ItemDTO(repository.findByItem(item));
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorItem(ItemEntity item){
        return this.converterLista(repository.findAllByItem(item));
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorItemIn(List<ItemEntity> itemList){
        return this.converterLista(repository.findAllByItemIn(itemList));
    }

    public Inventario_X_ItemDTO buscarPorQuantidade (Integer quantidade){
        return new Inventario_X_ItemDTO(repository.findByQuantidade(quantidade));
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorQuantidade (Integer quantidade){
        return this.converterLista(repository.findAllByQuantidade(quantidade));
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorQuantidadeIn (List<Integer> quantidadeList){
        return this.converterLista(repository.findAllByQuantidadeIn(quantidadeList));
    }

}
