package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public abstract interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository<T, Integer> {
    Optional<T> findById(Integer id);
    List<T> findAllById(Integer id);
    List<T> findAllByIdIn(List<Integer> ids);
    T findByNome( String nome );
    List<T> findAllByNome(String nome);
    List<T> findAllByNomeIn(List<String> nomeList);
    T findByExperiencia (Integer experiencia);
    List<T> findAllByExperiencia (Integer experiencia);
    List<T> findAllByExperienciaIn (List<Integer> experienciaList);
    T findByVida( Double vida );
    List<T> findAllByVida ( Double vida);
    List<T> findAllByVidaIn (List<Double> vidaList);
    T findByQtdDano ( Double dano);
    List<T> findAllByQtdDano ( Double dano);
    List<T> findAllByQtdDanoIn ( List<Double> danoList);
    T findByQtdExperienciaPorAtaque( Integer expPorAtk );
    List<T> findAllByQtdExperienciaPorAtaque ( Integer expPorAtk);
    List<T> findAllByQtdExperienciaPorAtaqueIn ( List<Integer> expPorAtkList);
    T findByInventario(InventarioEntity inventario);
    List<T> findAllByInventario(InventarioEntity inventario);
    List<T> findAllByInventarioIn(List<InventarioEntity> inventarioList);

}
