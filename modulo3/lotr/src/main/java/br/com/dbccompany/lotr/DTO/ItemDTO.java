package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;

import java.util.List;

public class ItemDTO {
    private String descricao;
    private List<Inventario_X_Item> inventario;


    public ItemDTO(ItemEntity item) {
        this.inventario = item.getInventarioItem();
        this.descricao = item.getDescricao();
    }

    public ItemDTO() {
    }

    public ItemEntity converter(){
        ItemEntity item = new ItemEntity();
        item.setDescricao(this.descricao);
        item.setInventarioItem(inventario);
        return item;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Inventario_X_Item> getInventario() {
        return inventario;
    }

    public void setInventario(List<Inventario_X_Item> inventario) {
        this.inventario = inventario;
    }
}
