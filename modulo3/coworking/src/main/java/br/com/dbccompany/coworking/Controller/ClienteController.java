package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    ClienteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClientesEntity> todosClientes() {
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ClienteDTO salvar(@RequestBody ClienteDTO clienteDTO) throws NoSuchAlgorithmException {
        ClientesEntity clienteEntity = clienteDTO.convert();
        ClienteDTO novoDTO = new ClienteDTO(service.salvar(clienteEntity));
        return novoDTO;
    }

    @PutMapping(value ="/editar/{id}")
    @ResponseBody
    public ClienteDTO editarCliente(@RequestBody ClienteDTO clienteDTO, @PathVariable Integer id ){
        ClientesEntity clienteEntity = clienteDTO.convert();
        ClienteDTO novoDTO = new ClienteDTO(service.editar(clienteEntity, id));
        return novoDTO;
    }

    @GetMapping(value = "/apenas/{id}")
    @ResponseBody
    public ClienteDTO apenasUmCliente(@PathVariable Integer id) {
        ClienteDTO clienteDTO = new ClienteDTO(service.porId(id));
        return clienteDTO;
    }



}
