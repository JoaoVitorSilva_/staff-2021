package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Date;
import java.util.Calendar;

public class AcessosDTO {
    private Integer id;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;

    private boolean isEntrada;
    private Date data;
    private  boolean isExcecao;

    {
        this.isEntrada = false;
        this.isExcecao = false;
    }

    public AcessosDTO() {}

    public AcessosDTO(AcessosEntity acessos) {
        this.id = acessos.getId();
        this.saldoCliente = acessos.getSaldoCliente();
        this.isEntrada = acessos.isIs_entrada();
        if(acessos.getData() == null){
            acessos.setData((Date) Calendar.getInstance().getTime());
        }
        this.data = acessos.getData();
        this.isExcecao = acessos.isIs_excecao();
    }

    public AcessosEntity convert() {
        AcessosEntity acesso = new AcessosEntity();
        acesso.setId(this.id);
        acesso.setSaldoCliente(this.saldoCliente);
        acesso.setIs_entrada(this.isEntrada);
        acesso.setData(data);
        acesso.setIs_excecao(this.isExcecao);
        return acesso;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}
