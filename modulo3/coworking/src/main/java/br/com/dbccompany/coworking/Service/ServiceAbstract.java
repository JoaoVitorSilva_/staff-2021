package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.Entity.EntityAbstract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public abstract class ServiceAbstract<R extends CrudRepository<E, T>, E extends EntityAbstract, T> {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    R repository;

    @Transactional(rollbackFor = Exception.class)
    public E salvar(E entidade) throws NoSuchAlgorithmException {
        try {
            return repository.save(entidade);
        } catch( Exception e ) {
            logger.error("Erro ao salvar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public E editar(E entidade, T id) {
        try {
            return repository.save(entidade);
        } catch ( Exception e ) {
            logger.error("Erro ao editar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public List<E> todos() {
        return (List<E>) repository.findAll();
    }

    public E porId(T id) {
        return repository.findById(id).get();
    }

}
