package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ContratacaoEntity> todasContratacoes(){
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ContratacaoDTO salvar(@RequestBody ContratacaoDTO contrato) throws NoSuchAlgorithmException {
        ContratacaoEntity contratacaoEntity = contrato.convert();
        ContratacaoDTO novoDTO = new ContratacaoDTO(service.salvar(contratacaoEntity));
        return novoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContratacaoDTO editarContrato(@RequestBody ContratacaoDTO contratacaoDTO, @PathVariable Integer id) {
        ContratacaoEntity contratacao = contratacaoDTO.convert();
        ContratacaoDTO novoDTO = new ContratacaoDTO(service.editar(contratacao, id));
        return novoDTO;
    }

    @GetMapping(value = "/apenas/{id}")
    @ResponseBody
    public ContratacaoDTO apenasUmaContratacao(@PathVariable Integer id){
        ContratacaoDTO contratacaoDTO = new ContratacaoDTO(service.porId(id));
        return contratacaoDTO;
    }

}
