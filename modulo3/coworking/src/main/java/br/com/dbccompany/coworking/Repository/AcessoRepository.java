package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AcessoRepository extends CrudRepository<AcessosEntity, Integer> {
    List<AcessosEntity> findAll();
    Optional<AcessosEntity> findById(Integer id);
}
