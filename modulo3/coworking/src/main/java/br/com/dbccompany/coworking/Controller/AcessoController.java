package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.AcessosDTO;
import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/acesso")
public class AcessoController {

    @Autowired
    AcessoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<AcessosEntity> todosAcessos() {
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public AcessosDTO salvar(@RequestBody AcessosDTO acessoDTO) throws NoSuchAlgorithmException {
        return service.salvarEntrada(acessoDTO);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public AcessosEntity editarAcesso(@PathVariable Integer id, @RequestBody AcessosEntity acesso){
        return service.editar(acesso, id);
    }

    @GetMapping(value = "/apenas/{id}")
    @ResponseBody
    public AcessosEntity apenasUmAcesso(@PathVariable Integer id){
        return service.porId(id);
    }


}
