package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class LogsGenerator {
    private static Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);
    private static RestTemplate restTemplate = new RestTemplate();
    private static String url = "http://localhost:8081/apiLogs/salvar";

    public static void enviaErro400( Exception e ){
        String mensagemLogger = "Dados inválidos";
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        ErrorDTO erroDTO = new ErrorDTO( e, "ERROR", mensagemLogger, "400" );
        restTemplate.postForObject(url, erroDTO, Object.class);
    }

    public static void enviaErro404( Exception e ){
        String mensagemLogger = "Não foi possivel encontrar o Objeto!";
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        ErrorDTO erroDTO = new ErrorDTO( e, "ERROR", mensagemLogger, "404" );
        restTemplate.postForObject(url, erroDTO, Object.class);
    }
}
