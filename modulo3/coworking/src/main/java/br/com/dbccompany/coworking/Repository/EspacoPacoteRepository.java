package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
    List<EspacoPacoteEntity> findAll();
    Optional<EspacoPacoteEntity> findById(Integer id);
}
