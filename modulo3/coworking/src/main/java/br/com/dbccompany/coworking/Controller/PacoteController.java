package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping(value = "/api/pacote")
public class PacoteController {

    @Autowired
    PacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PacoteEntity> todosPacotes(){
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public PacoteDTO salvar(@RequestBody PacoteDTO pacoteDTO) throws NoSuchAlgorithmException {
        PacoteEntity pacoteEntity = pacoteDTO.convert();
        PacoteDTO novoDTO = new PacoteDTO(service.salvar(pacoteEntity));
        return novoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public PacoteDTO editarPacote(@RequestBody PacoteDTO pacoteDTO, @PathVariable Integer id) {
        PacoteEntity pacoteEntity = pacoteDTO.convert();
        PacoteDTO novoDTO = new PacoteDTO(service.editar(pacoteEntity, id));
        return novoDTO;
    }

    @GetMapping(value = "/apenas/{id}")
    @ResponseBody
    public  PacoteDTO apenasUmPacote(@PathVariable Integer id) {
        PacoteDTO pacoteDTO = new PacoteDTO(service.porId(id));
        return pacoteDTO;
    }

}
