package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientePacoteRepository extends CrudRepository<ClientePacoteEntity, Integer> {
    List<ClientePacoteEntity> findAll ();
    Optional<ClientePacoteEntity> findById(Integer id);
}
