package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {
    List<PagamentoEntity> findAll ();
    Optional<PagamentoEntity> findById(Integer id);
    List<PagamentoEntity> findAllByTipoPagamento(TipoPagamentoEnum tipo );
}
