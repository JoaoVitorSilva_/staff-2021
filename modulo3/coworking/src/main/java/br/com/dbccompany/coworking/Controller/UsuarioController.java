package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping(value = "/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<UsuarioEntity> todosUsuarios() {
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public UsuarioDTO salvar(@RequestBody UsuarioDTO usuarioDTO) throws NoSuchAlgorithmException {
        UsuarioEntity usuarioEntity = usuarioDTO.convert();
        UsuarioDTO novoDTO = new UsuarioDTO(service.salvar(usuarioEntity));
        return novoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public UsuarioDTO editarUsuario(@RequestBody UsuarioDTO usuarioDTO, @PathVariable Integer id) {
        UsuarioEntity usuario = usuarioDTO.convert();
        UsuarioDTO novoDTO = new UsuarioDTO(service.editar(usuario, id));
        return novoDTO;
    }

    @PostMapping(value = "/login")
    @ResponseBody
    public boolean login(@RequestBody UsuarioDTO login) throws NoSuchAlgorithmException {
        return service.login(login.getLogin(), login.getSenha());
    }

}
