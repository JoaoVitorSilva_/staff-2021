package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;

import java.util.List;

public class ContratacaoDTO extends ContratacaoEntity {
    private Integer id;
    private EspacoEntity espaco;
    private ClientesEntity cliente;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private Integer prazo;
    private List<PagamentoEntity> pagamentos;

    public ContratacaoDTO() {}

    public ContratacaoDTO(ContratacaoEntity contrato) {
        this.id = contrato.getId();
        this.espaco = contrato.getEspacos();
        this.cliente = contrato.getClientes();
        this.tipoContratacao = contrato.getTipoContratacao();
        this.quantidade = contrato.getQuantidade();
        this.desconto = contrato.getDesconto();
        this.prazo = contrato.getPrazo();
        this.pagamentos = contrato.getPagamentos();
    }

    public ContratacaoEntity convert() {
        ContratacaoEntity contrato = new ContratacaoEntity();
        contrato.setId(this.id);
        contrato.setEspacos(this.espaco);
        contrato.setClientes(this.cliente);
        contrato.setTipoContratacao(this.tipoContratacao);
        contrato.setQuantidade(this.quantidade);
        contrato.setDesconto(this.desconto);
        contrato.setPrazo(this.prazo);
        contrato.setPagamentos(this.pagamentos);
        return contrato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}


