package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService extends ServiceAbstract<ClientesRepository, ClientesEntity, Integer> {

    @Autowired
    ContatoRepository contatoRepository;

    private ContatoEntity validacaoContato( String nome ) {
        ContatoEntity contato = contatoRepository.findByValor(nome);
        if( contato == null ){
            contato = contatoRepository.save(new ContatoEntity());
        }
        return contato;
    }

    public ClienteDTO salvarComContatos( ClienteDTO cliente ) {
        List<ContatoEntity> contato = new ArrayList<>();
        contato.add(validacaoContato("email"));
        contato.add(validacaoContato("telefone"));
        cliente.setContatos(contato);
        ClientesEntity clienteValidado = cliente.convert();
        ClienteDTO novaDTO = new ClienteDTO(repository.save(clienteValidado));
        return novaDTO;
    }


}