package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientePacoteController {
    @Autowired
    ClientePacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClientePacoteEntity> buscarTodos() {
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ClientePacoteDTO salvar(@RequestBody ClientePacoteDTO clientePacoteDTO) throws NoSuchAlgorithmException {
        ClientePacoteEntity clientePacoteEntity = clientePacoteDTO.convert();
        ClientePacoteDTO novoDTO = new ClientePacoteDTO(service.salvar(clientePacoteEntity));
        return novoDTO;
    }
}
