package br.com.dbccompany.coworking.DTO;


import br.com.dbccompany.coworking.Entity.PacoteEntity;

public class PacoteDTO {
    private Integer id;
    private int valor;

    public PacoteDTO(){}

    public PacoteDTO(PacoteEntity pacote) {
        this.id = pacote.getId();
        this.valor = pacote.getValor();
    }

    public PacoteEntity convert(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setId(this.id);
        pacote.setValor(this.valor);
        return pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
}
