package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

public class ContatoDTO {

    private Integer id;
    private TipoContatoEntity tipoContato;
    private ClientesEntity clientes;
    private String valor;

    public ContatoDTO() {}

    public ContatoDTO(ContatoEntity contato) {
        this.id = contato.getId();
        this.clientes = contato.getCliente();
        this.tipoContato = contato.getTipoContato();
        this.valor = contato.getValor();
    }

    public ContatoEntity convert() {
        ContatoEntity contato = new ContatoEntity();
        contato.setId(this.id);
        contato.setTipoContato(this.tipoContato);
        contato.setCliente(this.clientes);
        contato.setValor(this.valor);
        return contato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
