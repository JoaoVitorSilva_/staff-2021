package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ClientePacoteEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "CLIENTE_PACOTES_SEQ", sequenceName = "CLIENTE_PACOTES_SEQ")
    @GeneratedValue(generator = "CLIENTE_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private int quantidade;

    @ManyToOne
    @JoinColumn( name = "ID_CLIENTE" )
    private ClientesEntity clientes;

    @ManyToOne
    @JoinColumn( name = "ID_PACOTES" )
    private PacoteEntity pacotes;

    @OneToMany(mappedBy = "clientesPacotes")
    private List <PagamentoEntity> pagamentos;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public PacoteEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacoteEntity pacotes) {
        this.pacotes = pacotes;
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
