package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContratacaoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false)
    private Double desconto;

    @Column(nullable = false)
    private Integer prazo;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TipoContratacaoEnum tipoContratacao;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_espacos")
    private EspacoEntity espacos;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_clientes")
    private ClientesEntity clientes;

    @OneToMany(mappedBy = "contratacao")
    private List<PagamentoEntity> pagamentos;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public EspacoEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacoEntity espacos) {
        this.espacos = espacos;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
