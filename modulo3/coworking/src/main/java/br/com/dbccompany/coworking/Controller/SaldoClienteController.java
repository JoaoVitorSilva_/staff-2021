package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping(value = "/api/saldoCliente")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<SaldoClienteEntity> todosClientesComSaldo() {
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public SaldoClienteDTO salvar(@RequestBody SaldoClienteDTO saldoClienteDTO) throws NoSuchAlgorithmException {
        SaldoClienteEntity saldoClienteEntity = saldoClienteDTO.convert();
        SaldoClienteDTO novoDTO = new SaldoClienteDTO(service.salvar(saldoClienteEntity));
        return novoDTO;
    }

    @PutMapping(value = "/editar/{idCliente}/{idEspaco}")
    @ResponseBody
    public SaldoClienteDTO editarSaldoCliente(@RequestBody SaldoClienteDTO saldoClienteDTO, @PathVariable Integer idCliente , @PathVariable Integer idEspaco) {
        SaldoClienteEntity saldoCliente = saldoClienteDTO.convert();
        SaldoClienteDTO novoDTO = new SaldoClienteDTO(service.editar(saldoCliente, idCliente, idEspaco));
        return novoDTO;
    }
}
