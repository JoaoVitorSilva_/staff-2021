package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/espacoPacote")
public class EspacoPacoteController {

    @Autowired
    EspacoPacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacoPacoteEntity> todosEspacosPacotes(){
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public EspacoPacoteDTO salvar(@RequestBody EspacoPacoteDTO espacoPacoteDTO) throws NoSuchAlgorithmException {
        EspacoPacoteEntity espacoPacoteEntity = espacoPacoteDTO.convert();
        EspacoPacoteDTO novoDTO = new EspacoPacoteDTO(service.salvar(espacoPacoteEntity));
        return novoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacoPacoteDTO editarEspacoPacote(@RequestBody EspacoPacoteDTO espacoPacoteDTO, @PathVariable Integer id) {
        EspacoPacoteEntity espacoPct = espacoPacoteDTO.convert();
        EspacoPacoteDTO novoDTO = new EspacoPacoteDTO(service.editar(espacoPct, id));
        return novoDTO;
    }

    @GetMapping(value = "/apenas/{id}")
    @ResponseBody
    public EspacoPacoteDTO apenasUmEspacoPacote(@PathVariable Integer id) {
        EspacoPacoteDTO espacoPacoteDTO = new EspacoPacoteDTO(service.porId(id));
        return  espacoPacoteDTO;
    }

}
