package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class EspacoPacoteEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "ESPACO_PACOTES_SEQ", sequenceName = "ESPACO_PACOTES_SEQ")
    @GeneratedValue(generator = "ESPACO_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private int quantidade;
    private int prazo;

    @Enumerated(EnumType.STRING)
    private TipoContratacaoEnum tipoContratacao;

    @ManyToOne
    @JoinColumn(name = "id_pacote", nullable = false)
    private PacoteEntity pacotes;

    @ManyToOne
    @JoinColumn(name = "id_espaco", nullable = false)
    private EspacoEntity espacos;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public PacoteEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacoteEntity pacotes) {
        this.pacotes = pacotes;
    }

    public EspacoEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacoEntity espacos) {
        this.espacos = espacos;
    }
}
