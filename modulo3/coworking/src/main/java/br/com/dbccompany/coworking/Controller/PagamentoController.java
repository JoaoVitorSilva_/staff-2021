package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping(value = "/api/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PagamentoEntity> todosPagamentos() {
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public PagamentoDTO salvar(@RequestBody PagamentoDTO pagamentoDTO) throws NoSuchAlgorithmException {
        PagamentoEntity pagamentoEntity = pagamentoDTO.convert();
        PagamentoDTO novoDTO = new PagamentoDTO(service.salvar(pagamentoEntity));
        return novoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public PagamentoDTO editarPagamento(@RequestBody PagamentoDTO pagamentoDTO, @PathVariable Integer id) {
        PagamentoEntity pagamento = pagamentoDTO.convert();
        PagamentoDTO novoDTO = new PagamentoDTO(service.editar(pagamento, id));
        return novoDTO;
    }

    @GetMapping(value = "/apenas/{id}")
    @ResponseBody
    public PagamentoDTO apenasUmPagamento(@PathVariable Integer id) {
        PagamentoDTO pagamentoDTO = new PagamentoDTO(service.porId(id));
        return pagamentoDTO;
    }
}
