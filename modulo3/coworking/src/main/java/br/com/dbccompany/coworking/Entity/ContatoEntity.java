package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ContatoEntity extends EntityAbstract<Integer> implements Serializable {

    @Id
    @SequenceGenerator(name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String valor;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_tipo_contato")
    private TipoContatoEntity tipoContato;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CLIENTES")
    private ClientesEntity clientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClientesEntity getCliente() {
        return clientes;
    }

    public void setCliente(ClientesEntity cliente) {
        this.clientes = cliente;
    }
}


