package br.com.dbccompany.coworking.Util;


import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

public class ConverterUnidadesParaMinutosUtil {
    public static int converteParaMinutos(int quantidade, TipoContratacaoEnum tipoContratacaoEnum)  {
        int qtdMinutos = 0;
        switch (tipoContratacaoEnum) {
            case MES:
                qtdMinutos = 43200;
                break;
            case SEMANA:
                qtdMinutos = 7200;
                break;
            case DIARIA:
                qtdMinutos = 1440;
                break;
            case TURNO:
                qtdMinutos = 300;
                break;
            case HORA:
                qtdMinutos = 60;
                break;
            case MINUTO:
                qtdMinutos = 1;
                break;
        }
        return (int) quantidade * qtdMinutos;
    }
}