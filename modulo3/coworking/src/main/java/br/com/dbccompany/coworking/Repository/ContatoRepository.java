package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    List<ContatoEntity> findAll();
    Optional<ContatoEntity> findById(Integer id);
    ContatoEntity findByValor(String nome);
}
