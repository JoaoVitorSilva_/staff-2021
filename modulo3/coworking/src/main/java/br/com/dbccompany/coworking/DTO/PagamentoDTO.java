package br.com.dbccompany.coworking.DTO;


import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;

public class PagamentoDTO {
    private Integer id;
    private ClientePacoteEntity clientePacote;
    private ContratacaoEntity contratao;
    private TipoPagamentoEnum tipoPagamento;

    public PagamentoDTO(){}

    public PagamentoDTO(PagamentoEntity pagamento) {
        this.id = pagamento.getId();
        this.clientePacote = pagamento.getClientesPacotes();
        this.contratao = pagamento.getContratacao();
        this.tipoPagamento = pagamento.getTipoPagamento();
    }

    public PagamentoEntity convert() {
        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setId(this.id);
        pagamento.setClientesPacotes(this.clientePacote);
        pagamento.setContratacao(this.contratao);
        pagamento.setTipoPagamento(this.tipoPagamento);
        return pagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratao() {
        return contratao;
    }

    public void setContratao(ContratacaoEntity contratao) {
        this.contratao = contratao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
