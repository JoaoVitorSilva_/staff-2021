package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientePacoteService extends ServiceAbstract<ClientePacoteRepository, ClientePacoteEntity, Integer>{
}
