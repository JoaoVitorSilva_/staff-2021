package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer>{

    private final Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity salvar(UsuarioEntity usuario) throws NoSuchAlgorithmException {
        String senha = usuario.getSenha();
        logger.info("Sua senha deve possuir ao minino 6 caracteres!");
        if(senha.length() >= 6) {
            try {
                usuario.setSenha(new BCryptPasswordEncoder().encode(usuario.getSenha()));
                return repository.save(usuario);
            }catch (Exception e){
                logger.error("Erro ao salvar usuario: " + e.getMessage());
                throw new RuntimeException();
            }
        }
        logger.warn("Você digitou uma senha menor que 6 caracteres!");
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity trocarSenha(UsuarioEntity usuario, Integer id, String novaSenha) throws NoSuchAlgorithmException {
        logger.info("Sua nova senha deve possuir ao minino 6 caracteres!");
        if(novaSenha.length() >= 6) {
            try {
                usuario.setSenha(new BCryptPasswordEncoder().encode(novaSenha));
                return repository.save(usuario);
            }catch (Exception e){
                logger.error("Erro ao editar usuario: " + e.getMessage());
                throw new RuntimeException();
            }
        }
        logger.warn("Você digitou uma nova senha menor que 6 caracteres!");
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean login(String login, String senha) throws NoSuchAlgorithmException {
        logger.info("Buscar usuario com login e senha salvos!");
        UsuarioEntity usuario = repository.findByLoginAndSenha(login, new BCryptPasswordEncoder().encode(senha));
        return usuario != null;
    }
}
