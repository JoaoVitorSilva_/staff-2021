package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PagamentoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoPagamentoEnum tipoPagamento;

    @ManyToOne
    @JoinColumn(name = "id_contratacao", nullable = false)
    private ContratacaoEntity contratacao;

    @ManyToOne
    @JoinColumn(name = "id_cliente_pacote", nullable = false)
    private ClientePacoteEntity clientesPacotes;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public ClientePacoteEntity getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientePacoteEntity clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
