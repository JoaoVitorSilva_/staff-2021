package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

public class EspacoPacoteDTO {
    private Integer id;
    private EspacoEntity espacos;
    private PacoteEntity pacotes;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private int prazo;

    public EspacoPacoteDTO() {}

    public EspacoPacoteDTO(EspacoPacoteEntity espacoPacote) {
        this.id = espacoPacote.getId();
        this.espacos = espacoPacote.getEspacos();
        this.pacotes = espacoPacote.getPacotes();
        this.tipoContratacao = espacoPacote.getTipoContratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
    }

    public EspacoPacoteEntity convert(){
        EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();
        espacoPacote.setId(this.id);
        espacoPacote.setEspacos(this.espacos);
        espacoPacote.setPacotes(this.pacotes);
        espacoPacote.setTipoContratacao(this.tipoContratacao);
        espacoPacote.setQuantidade(this.quantidade);
        espacoPacote.setPrazo(this.prazo);
        return espacoPacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacoEntity espacos) {
        this.espacos = espacos;
    }

    public PacoteEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacoteEntity pacotes) {
        this.pacotes = pacotes;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }
}
