package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {

    @Column(name = "id_cliente")
    private int idClientes;

    @Column(name = "id_espaco")
    private int idEspacos;

    public SaldoClienteId() {}

    public SaldoClienteId(int idEspacos, int idClientes) {
        this.idEspacos = idEspacos;
        this.idClientes = idClientes;
    }

    public int getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(int idClientes) {
        this.idClientes = idClientes;
    }

    public int getIdEspacos() {
        return idEspacos;
    }

    public void setIdEspacos(int idEspacos) {
        this.idEspacos = idEspacos;
    }
}
