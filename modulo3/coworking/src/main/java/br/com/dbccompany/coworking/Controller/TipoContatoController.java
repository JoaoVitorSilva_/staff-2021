package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping(value = "/api/tipoContato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<TipoContatoEntity> todosTipoDeContato() {
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public TipoContatoDTO salvar(@RequestBody TipoContatoDTO tipoContatoDTO) throws NoSuchAlgorithmException {
        TipoContatoEntity tipoContatoEntity = tipoContatoDTO.convert();
        TipoContatoDTO novoDTO = new TipoContatoDTO(service.salvar(tipoContatoEntity));
        return novoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoContatoDTO editarTipoContato(@RequestBody TipoContatoDTO tipoContatoDTO, @PathVariable Integer id) {
        TipoContatoEntity tipoContato = tipoContatoDTO.convert();
        TipoContatoDTO novoDTO = new TipoContatoDTO(service.editar(tipoContato, id));
        return novoDTO;
    }

    @GetMapping(value = "/apenas/{id}")
    @ResponseBody
    public TipoContatoDTO apenasUmTipoContato(@PathVariable Integer id){
        TipoContatoDTO tipoContatoDTO = new TipoContatoDTO(service.porId(id));
        return tipoContatoDTO;
    }
}
