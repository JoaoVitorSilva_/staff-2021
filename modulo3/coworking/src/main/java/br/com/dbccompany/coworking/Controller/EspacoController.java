package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController {

    @Autowired
    EspacoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacoEntity> todaosEspacos(){
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public EspacoDTO salvar(@RequestBody EspacoDTO espaco) throws NoSuchAlgorithmException {
        EspacoEntity espacoEntity = espaco.convert();
        EspacoDTO novoDTO = new EspacoDTO(service.salvar(espacoEntity));
        return novoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacoDTO editarEspaco(@RequestBody EspacoDTO espacoDTO, @PathVariable Integer id) {
        EspacoEntity espaco = espacoDTO.convert();
        EspacoDTO novoDTO = new EspacoDTO(service.editar(espaco, id));
        return novoDTO;
    }

    @GetMapping(value = "/apenas/{id}")
    @ResponseBody
    public EspacoDTO apenasUmaContratacao(@PathVariable Integer id){
        EspacoDTO espacoDTO = new EspacoDTO(service.porId(id));
        return espacoDTO;
    }
}
