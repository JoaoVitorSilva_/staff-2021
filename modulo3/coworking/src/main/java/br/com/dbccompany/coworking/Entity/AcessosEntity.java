package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class AcessosEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue( generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private boolean isEntrada;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Date data;

    private boolean isExcecao;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumns(value = {
            @JoinColumn(name = "ID_ESPACO_SALDO_CLIENTE", nullable = false),
            @JoinColumn(name = "ID_CLIENTE_SALDO_CLIENTE", nullable = false)
    } )

    private SaldoClienteEntity saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }


    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isIs_entrada() {
        return isEntrada;
    }

    public void setIs_entrada(boolean is_entrada) {
        this.isEntrada = is_entrada;
    }

    public boolean isIs_excecao() {
        return isExcecao;
    }

    public void setIs_excecao(boolean is_excecao) {
        this.isExcecao = is_excecao;
    }
}

