package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClientesRepository extends CrudRepository<ClientesEntity, Integer> {
    List<ClientesEntity> findAll ();
    Optional<ClientesEntity> findById(Integer id);
    ClientesEntity findByNome( String nome );
    List<ClientesEntity> findAllByNome( String nome );
    ClientesEntity findByCpf( String cpf );
    List<ClientesEntity> findAllByCpf( String cpf );
    ClientesEntity findByNomeAndCpf(String nome, String cpf);
    List<ClientesEntity> findAllByDataNascimento(Date dataNascimento);
}
