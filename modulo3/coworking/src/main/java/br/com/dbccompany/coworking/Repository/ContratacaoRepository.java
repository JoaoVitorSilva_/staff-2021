package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    List<ContratacaoEntity> findAll ();
    Optional<ContratacaoEntity> findById(Integer id);
    List<ContratacaoEntity> findAllByTipoContratacao(TipoContratacaoEnum tipoContratacao);
    ContratacaoEntity findByPrazo(Integer prazo);
}
