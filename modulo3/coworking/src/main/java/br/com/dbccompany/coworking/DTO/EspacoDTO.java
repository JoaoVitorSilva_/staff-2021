package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.EspacoEntity;

public class EspacoDTO {
    private Integer id;
    private String nome;
    private int qtdPessoas;
    private Double valor;

    public EspacoDTO() {}

    public EspacoDTO(EspacoEntity espaco){
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qtdPessoas = espaco.getQtdPessoas();
        this.valor = espaco.getValor();
    }

    public EspacoEntity convert(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setId(this.id);
        espaco.setNome(this.nome);
        espaco.setQtdPessoas(this.qtdPessoas);
        espaco.setValor(this.valor);
        return espaco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
