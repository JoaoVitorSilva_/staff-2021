package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

    @Autowired
    ContatoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ContatoEntity> todosContato(){
        return service.todos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ContatoDTO salvar(@RequestBody ContatoDTO contatoDTO) throws NoSuchAlgorithmException {
        ContatoEntity contatoEntity = contatoDTO.convert();
        ContatoDTO novoDTO = new ContatoDTO(service.salvar(contatoEntity));
        return novoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContatoDTO editarCliente(@RequestBody ContatoDTO contatoDTO, @PathVariable Integer id) {
        ContatoEntity contato = contatoDTO.convert();
        ContatoDTO novoDTO = new ContatoDTO(service.editar(contato, id));
        return novoDTO;
    }

    @GetMapping(value = "/apenas/{id}")
    @ResponseBody
    public ContatoDTO apenasUmContato(@PathVariable Integer id) {
        ContatoDTO contatoDTO = new ContatoDTO(service.porId(id));
        return contatoDTO;
    }


}
