package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "SALDO_CLIENTE")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ClientesEntity.class)
public class SaldoClienteEntity extends EntityAbstract<SaldoClienteId> {

    @EmbeddedId
    private SaldoClienteId id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Date vencimento;

    @ManyToOne
    @JoinColumn(name = "id_cliente", nullable = false, insertable = false, updatable = false)
    private ClientesEntity clientes;

    @ManyToOne
    @JoinColumn(name = "id_espaco", nullable = false, insertable = false, updatable = false)
    private EspacoEntity espacos;

    @OneToMany( mappedBy = "saldoCliente")
    private List<AcessosEntity> acesso;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public ClientesEntity getCliente() {
        return clientes;
    }

    public void setCliente(ClientesEntity cliente) {
        this.clientes = cliente;
    }

    public EspacoEntity getEspaco() {
        return espacos;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espacos = espaco;
    }

    public List<AcessosEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessosEntity> acesso) {
        this.acesso = acesso;
    }
}
