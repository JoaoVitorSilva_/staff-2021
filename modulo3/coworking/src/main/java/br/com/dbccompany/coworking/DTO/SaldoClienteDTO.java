package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;

import java.sql.Date;
import java.util.List;

public class SaldoClienteDTO {
    private SaldoClienteId id;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private Date vencimento;
    private ClientesEntity cliente;
    private EspacoEntity espaco;
    private List<AcessosEntity> acesso;

    public SaldoClienteDTO(){}

    public SaldoClienteDTO(SaldoClienteEntity saldoCliente) {
        this.id = saldoCliente.getId();
        this.tipoContratacao = saldoCliente.getTipoContratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
        this.cliente = saldoCliente.getCliente();
        this.espaco = saldoCliente.getEspaco();
        this.acesso = saldoCliente.getAcesso();
    }

    public SaldoClienteEntity convert() {
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(this.id);
        saldoCliente.setTipoContratacao(this.tipoContratacao);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setVencimento(this.vencimento);
        saldoCliente.setCliente(this.cliente);
        saldoCliente.setEspaco(this.espaco);
        saldoCliente.setAcesso(this.acesso);
        return saldoCliente;
    }

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessosEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessosEntity> acesso) {
        this.acesso = acesso;
    }
}
