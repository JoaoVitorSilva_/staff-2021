package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue(generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private int valor;

    @OneToMany(mappedBy = "pacotes")
    private List<ClientePacoteEntity> clientesPacotes;

    @OneToMany(mappedBy = "pacotes")
    private List<EspacoPacoteEntity> espacoPacote;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public List<ClientePacoteEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientePacoteEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<EspacoPacoteEntity> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<EspacoPacoteEntity> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }
}
