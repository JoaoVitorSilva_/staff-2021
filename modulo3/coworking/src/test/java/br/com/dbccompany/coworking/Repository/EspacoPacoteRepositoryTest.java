package br.com.dbccompany.coworking.Repository;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EspacoPacoteRepositoryTest {

    @Autowired
    EspacoRepository espacoRep;
    @Autowired
    PacoteRepository pacoteRep;
    @Autowired
    EspacoPacoteRepository repository;

    @Test
    @Order(1)
    public void buscarEspacosPacotesQueNaoExiste(){
        assertEquals(Optional.empty(),repository.findById(2));
    }
}
