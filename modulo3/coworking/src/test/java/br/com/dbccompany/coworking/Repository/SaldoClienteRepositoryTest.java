package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SaldoClienteRepositoryTest {

    @Autowired
    SaldoClienteRepository repository;
    @Autowired
    EspacoRepository espacoRep;
    @Autowired
    ClientesRepository clienteRep;

   /* @Test
    @Order(1)
    public void salvarSaldoClienteBuscarPorId(){

        SaldoClienteId id = new SaldoClienteId();
        id.setIdClientes(1);
        id.setIdEspacos(1);

        ClientesEntity cliente = new ClientesEntity();
        EspacoEntity espaco = new EspacoEntity();
        espacoRep.save(espaco);
        clienteRep.save(cliente);

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();

        saldoCliente.setId(id);
        saldoCliente.setCliente(cliente);
        saldoCliente.setEspaco(espaco);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.HORA);
        saldoCliente.setQuantidade(3);
        saldoCliente.setVencimento(new Date(2001,10,27));

        repository.save(saldoCliente);

        assertEquals(1, repository.findById(id).get().getId().getIdClientes());
    }*/

    @Test
    @Order(1)
    public void buscarSaldoClientesQueNaoExistem(){
        SaldoClienteId id = new SaldoClienteId();
        id.setIdClientes(1);
        id.setIdEspacos(1);
        assertEquals(Optional.empty(),repository.findById(id));
    }

}
