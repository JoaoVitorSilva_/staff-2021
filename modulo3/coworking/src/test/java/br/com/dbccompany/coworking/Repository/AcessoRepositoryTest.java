package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AcessoRepositoryTest {

    @Autowired
    private AcessoRepository repository;
    @Autowired
    private EspacoRepository espacoRep;
    @Autowired
    private ClientesRepository clientesRep;
    @Autowired
    private SaldoClienteRepository saldoCliRep;

    @Test
    @Order(1)
    public void salvarAcesso(){

        SaldoClienteId id = new SaldoClienteId();
        id.setIdEspacos(1);
        id.setIdClientes(1);

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(id);

        AcessosEntity acesso = new AcessosEntity();
        acesso.setSaldoCliente(saldoCliente);
        acesso.setIs_entrada(true);
        acesso.setIs_excecao(false);
        acesso.setData(new Date(1992,10,10));

        repository.save(acesso);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    @Order(2)
    public void buscarAcessosInexistentes(){
        assertFalse(repository.findById(1).isPresent());
    }

}
