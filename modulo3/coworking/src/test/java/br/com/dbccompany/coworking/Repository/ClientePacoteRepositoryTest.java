package br.com.dbccompany.coworking.Repository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClientePacoteRepositoryTest {
    @Autowired
    ClientePacoteRepository repository;
    @Autowired
    ClientesRepository clientesRep;
    @Autowired
    PacoteRepository pacotesRep;

    /*@Test
    @Order(1)
    public void salvarClientesPacotesEBuscarPorId(){
        ClientesEntity cliente = new ClientesEntity();
        PacoteEntity pacotes = new PacoteEntity();

        clientesRep.save(cliente);
        pacotesRep.save(pacotes);

        ClientePacoteEntity cliPacote = new ClientePacoteEntity();

        cliPacote.setClientes(cliente);
        cliPacote.setPacotes(pacotes);
        cliPacote.setQuantidade(5);

        repository.save(cliPacote);

        assertEquals(0, repository.findById(1).get().getId());
    }*/

    @Test
    @Order(1)
    public void buscarClientesPacotesQueNaoExistem(){
        assertEquals(Optional.empty(),repository.findById(2));
    }
}
