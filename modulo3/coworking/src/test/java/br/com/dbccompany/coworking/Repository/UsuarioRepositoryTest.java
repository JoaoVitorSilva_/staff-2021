package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository repository;

    /*@Test
    public void salvarUsuario(){
        UsuarioEntity usuario = new UsuarioEntity();

        usuario.setNome("Joao");
        usuario.setEmail("coworkingDoJao@gmail.com");
        usuario.setLogin("joao");
        usuario.setSenha("123456");

        repository.save(usuario);
        assertEquals(usuario.getNome(),
                repository.findByNome("Mario").getNome()
        );
    }

    @Test
    public void buscarUsuarioPorUsernameEPassword(){
        UsuarioEntity usuario = new UsuarioEntity();

        usuario.setNome("Joao");
        usuario.setEmail("coworkingDoJao@gmail.com");
        usuario.setLogin("joao");
        usuario.setSenha("123456");

        repository.save(usuario);
        assertEquals(usuario.getNome(),
                repository.findByLoginAndSenha(
                        usuario.getUsername(),
                        usuario.getPassword()
                ).getNome()
        );
    }*/

    @Test
    public void buscarUsuarioQueNaoExiste(){
        String nome = "Jao";

        assertNull(repository.findByNome(nome));
    }

    @Test
    public void buscarPorLogin(){
        UsuarioEntity usuario = new UsuarioEntity();

        usuario.setNome("Joao");
        usuario.setEmail("coworkinDoJao@gmail.com");
        usuario.setLogin("joao");
        usuario.setSenha("123456");

        repository.save(usuario);
        assertEquals("Joao", repository.findByLogin("joao").get().getNome()
        );
    }
}
