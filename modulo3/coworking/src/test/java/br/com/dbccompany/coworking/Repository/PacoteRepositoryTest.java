package br.com.dbccompany.coworking.Repository;
;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PacoteRepositoryTest {

    @Autowired
    private PacoteRepository repository;

    @Test
    @Order(1)
    public void salvarPacoteBuscarPorId(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2);

        repository.save(pacote);// arrumar para ordenar execução
        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    @Order(2)
    public void buscarPacotePorValor(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2);

        repository.save(pacote);
        assertEquals(2, repository.findByValor(2).getValor());
    }

    @Test
    @Order(3)
    public void buscarPacotePorValorQueNaoExiste(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2);

        repository.save(pacote);
        assertNull(repository.findByValor(3));
    }

    @Test
    @Order(4)
    public void buscarPacotePorIdQueNaoExiste(){

        assertEquals(Optional.empty(),repository.findById(15));
    }

}
