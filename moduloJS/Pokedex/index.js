const pokeApi = new PokeApi(); // eslint-disable-line no-unused-vars
const pokemonUsed = [];

buscarPokemon = () => { // eslint-disable-line no-undef
  const idPokemon = document.getElementById( 'idPokemon' ).value;
  const erro = document.getElementById( 'erro' );
  pokeApi
    .buscarEspecifico( idPokemon ).then( pokemon => {
      erro.innerHTML = '';
      const poke = new Pokemon( pokemon )
      renderizar( poke ); // eslint-disable-line no-undef
    }, err => {
      erro.innerHTML = 'Digite um id válido';
      console.log( err );
    } );
}

estouComSorte = () => { // eslint-disable-line no-undef
  randomNumber = ( min, max ) => Math.floor( Math.random() // eslint-disable-line no-undef
  * ( max - min ) ) + min
  const buscaId = randomNumber( 1, 893 ); // eslint-disable-line no-undef
  const numUsed = pokemonUsed.filter( num => num === buscaId );
  if ( numUsed.length > 0 ) {
    return;
  }
  pokemonUsed.push( buscaId );

  pokeApi
    .buscarEspecifico( buscaId ).then( pokemon => {
      erro.innerHTML = ''; // eslint-disable-line no-undef
      const poke = new Pokemon( pokemon )
      renderizar( poke ); // eslint-disable-line no-undef
    }, err => {
      const erro = document.getElementById( 'erro' );
      erro.innerHTML = 'Digite um Id válido';
      console.log( err );
    } );
}

renderizar = ( pokemon ) => { // eslint-disable-line no-undef
  const dadosPokemon = document.getElementById( 'pokedex' );
  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = `Nome: ${ pokemon.nome }`;

  const pokeId = dadosPokemon.querySelector( '.pokeId' );
  pokeId.innerHTML = `N°: ${ pokemon.pokeID }`;
  idInicial = pokemon.pokeID; // eslint-disable-line no-undef

  const imgPokemon = dadosPokemon.querySelector( '.img' );
  imgPokemon.src = pokemon.imagem;
  imgPokemon.alt = `Img ${ pokemon.nome }`;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `Altura: ${ pokemon.altura }`;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = `Peso: ${ pokemon.peso }`;

  const tipos = dadosPokemon.querySelector( '.tipo' );
  tipos.innerHTML = 'Tipo: '
  const ul = document.createElement( 'ul' );
  tipos.appendChild( ul );
  // eslint-disable-next-line array-callback-return
  pokemon.tipos.map( tipo => {
    const li = document.createElement( 'li' );
    li.innerHTML = `${ tipo.type.name }`;
    ul.appendChild( li );
  } );

  const estatistica = dadosPokemon.querySelector( '.estatistica' );
  estatistica.innerHTML = 'Estatisticas: ';
  const ulStat = document.createElement( 'ul' );
  estatistica.appendChild( ulStat );
  // eslint-disable-next-line array-callback-return
  pokemon.estatistica.map( stats => {
    const li = document.createElement( 'li' );
    li.innerHTML = `${ stats.stat.name }
        -   ${ stats.base_stat }%`;
    ulStat.appendChild( li );
  } );
}
