class PokeApi { // eslint-disable-line no-unused-vars
  constructor() {
    this._url = 'https://pokeapi.co/api/v2/pokemon'
  }

  /*  then = executa qndo a promessa da sucesso, podem acontecer + de uma vez.
  catch = só cai no qndo a promessa da erro.  */

  buscarTodos() {
    const requisicao = fetch( `${ this._url }?limit=893&offset=200` )
    return requisicao.then( data => data.json().then( data.results ) );
  }

  buscarEspecifico( id ) {
    const requisicao = fetch( `${ this._url }/${ id }` );
    return requisicao.then( data => data.json() );
  }
}
