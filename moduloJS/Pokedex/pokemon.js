class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objDaApi ) {
    this._nome = objDaApi.name;
    this._imagem = objDaApi.sprites.front_default;
    this._pokeID = objDaApi.id;
    this._altura = objDaApi.height;
    this._peso = objDaApi.weight;
    this._tipos = objDaApi.types;
    this._estatistica = objDaApi.stats;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get pokeID() {
    return this._pokeID;
  }

  get altura() {
    return `${ this._altura * 10 } cm`
  }

  get peso() {
    return `${ this._peso / 10 } kg`;
  }

  get tipos() {
    return this._tipos;
  }

  get estatistica() {
    return this._estatistica;
  }
}
