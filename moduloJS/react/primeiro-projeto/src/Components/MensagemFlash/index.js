import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MensagemFlash extends Component {
  constructor( props ) {
    super(props);
    this.idsTimeout = [];
    this.animacao = '';
  }  

  fechar() {
    this.props.atualizar( false );
  }

  limparTimeouts() {
    this.idsTimeout.forEach( clearTimeout ); 
  }

  componentWillUnmount() {
    this.limparTimeouts();
  }

  componentDidUpdate( prevProps ) {
    const { exibir, segundos } = this.props;

    if( prevProps.exibir !== exibir ) {
      const novoIdTimeout = setTimeout(() => {
        this.fechar();
      }, segundos * 1000);

      this.idsTimeout.push( novoIdTimeout );
    }
  }
  
  render() {
    const { cor, mensagem, exibir } = this.props;

    if( this.animacao || exibir ) {
      this.animacao = exibir ? 'fade-in' : 'fade-out';
    }

    return <span onClick={ () => this.fechar() } className={ `flash ${ cor } ${ this.animacao } `}>{ mensagem }</span>;
  }
}

MensagemFlash.propTypes = {
  mensagem: PropTypes.string.isRequired,
  cor: PropTypes.oneOf( ['verde', 'vermelho'] )
}

MensagemFlash.defaultProps = {
  cor: 'verde',
  segundos: 3
}