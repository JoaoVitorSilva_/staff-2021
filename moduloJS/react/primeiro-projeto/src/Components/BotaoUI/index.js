import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const BotaoUi = ({ classe, metodo, nome, link }) => 
  <React.Fragment>
    <button className={ `btn ${ classe }` } onClick={ metodo } >
      { link ? <Link to={ link } >{ nome }</Link> : nome }
    </button>
  </React.Fragment>

BotaoUi.propTypes = {
  classe: PropTypes.string,
  nome: PropTypes.string.isRequired,
  metodo: PropTypes.func,
  link: PropTypes.string
}

export default BotaoUi;