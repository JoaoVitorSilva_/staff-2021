import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import EpisodioUi from '../Components/EpisodioUI';
import MensagemFlash from '../Components/MensagemFlash';
import MeuInputNumero from '../Components/MeuInputNumero';
import BotaoUi from '../Components/BotaoUI';

import ListaEpisodios from '../Models/ListaEpisodios';

import Mensagem from '../Constants/mensagem';

import './App.css';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    this.sortear = this.sortear.bind( this );
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio,
      deveExibirMensagem: false,
      deveExibirErro: false
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio;

    this.setState((state) => {
      return { 
            ...state,
            episodio
      }
    });
  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.assistido();
    this.setState((state) => {
      return { 
            ...state,
            episodio
      }
    });
  }

  registrarNota( { erro, nota } ) {
    this.setState((state) => {
      return { 
        ...state,
        deveExibirErro: erro
      }
    });
    if( erro ) {
      return;
    }
    
    const { episodio } = this.state;
    let mensagem, corMensagem;
    
    if( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota );
      corMensagem = 'verde';
      mensagem = Mensagem.SUCESSO.REGISTRO_NOTA;
    }else{
      corMensagem = 'vermelho';
      mensagem = Mensagem.ERRO.NOTA_INVALIDA;
    }

    this.exibirMensagem({ corMensagem, mensagem });
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState((state) => {
      return { 
            ...state,
            deveExibirMensagem: true,
            mensagem,
            corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState((state) => {
      return { 
            ...state,
            deveExibirMensagem: devoExibir
      }
    });
  }
  
  render() {
    const { episodio, deveExibirMensagem, corMensagem, mensagem, deveExibirErro } = this.state;
    const { listaEpisodios } = this;

    return (
      <div className="App">
        <MensagemFlash atualizar={ this.atualizarMensagem } exibir={ deveExibirMensagem } mensagem={ mensagem } cor={ corMensagem } />
        <header className="App-header">
          <Link to={{ pathname: "/avaliacoes", state: { listaEpisodios } }} >Lista de avaliações</Link>
          <EpisodioUi episodio={ episodio } />
          <div className="botoes">
            <BotaoUi classe="verde" metodo={ this.sortear } nome="Próximo" />
            <BotaoUi classe="azul" metodo={ this.marcarComoAssistido } nome="Já Assisti!" />
          </div>
          <span>Nota: { episodio.nota }</span>
          <MeuInputNumero
              placeholder="Nota de 1 a 5"
              mensagem={ Mensagem.DESCRICAO.INPUT_EPISODIO }
              visivel={ episodio.foiAssistido || false }
              obrigatorio
              exibirErro={ deveExibirErro }
              atualizarValor={ this.registrarNota.bind( this ) }/>
        </header>
      </div>
    );
  }
}