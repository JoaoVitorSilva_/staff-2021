import React from 'react';
import { Link } from 'react-router-dom';

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state;

  return (
    <React.Fragment>
      <Link to="/">Página Inicial</Link>
      {
        listaEpisodios.avaliados && (
          listaEpisodios.avaliados.map( ( e, i ) => {
            return <li key={ i }>{ `${ e.nome } - ${ e.nota }` }</li>;
          })
        )
      }
    </React.Fragment>
  );
} 

export default ListaAvaliacoes;