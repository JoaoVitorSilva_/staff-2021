const Mensagem = {
  SUCESSO: {
    REGISTRO_NOTA: 'Nota registrada com sucesso!'
  },
  ERRO: {
    NOTA_INVALIDA: 'Informar uma nota válida (entre 1 e 5)!',
    CAMPO_OBRIGATORIO: '* Obrigatório'
  },
  DESCRICAO: {
    INPUT_EPISODIO: 'Qual a sua nota para esse episódio?'
  }
}