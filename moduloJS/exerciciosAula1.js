let raio = {
    raio: 5,
    tipoCalculo: "C"
}

function calcularCirculo (raioParaCalcular) {

    function calcularArea(){
        return Math.ceil(Math.PI * Math.pow(raioParaCalcular.raio, 2));
    }

    function calcularCircunferencia(){
        return 2 * Math.PI * raioParaCalcular.raio;
    }

    let tipoCalculo = raioParaCalcular.tipoCalculo;

    if (tipoCalculo === "A") {
        return calcularArea();
    }
    if (tipoCalculo === "C"){
        return calcularCircunferencia();
    }   
}

console.log(calcularCirculo({raio: 10, tipoCalculo: "A"}));

/** --------------EXERCICIO 2 ------------------- */

function Bissexto( ano ) {
    return ( ano %400 == 0 ) || ( ano %4 == 0 && ano %100 !=0 ) ? true : false;
}

console.log(Bissexto(2000));
console.log(Bissexto(2015));
console.log(Bissexto(2020));


function somarPares(array = []){
    let resultado = 0;
    for(let i = 0; i < array.length; i++){
        if(i %2 == 0){
            resultado += array[i];
        }
    }
    return resultado;
}

console.log(somarPares([1, 2, 3, 4, 5, 6, 7]));

/** --------------EXERCICIO 3 ------------------- */


function adicionarModoUm( number1 ){

    function somar(number2) {
        return number1 + number2;
    }

    return somar;

}

/** --------------EXERCICIO 4 ------------------- */

let adicionar = valor1 => valor2 => valor1 + valor2;

console.log(adicionar(3)(4));


/** --------------EXERCICIO EXTRA ------------------- */


function imprimirBRL(value){
    if(value >= 0){
        let numeroParcialmenteFormatado =  value.toLocaleString('pt-BR');
        let array = numeroParcialmenteFormatado.split(",");
        let aux = 0;
    
        if (parseInt(array[1], 10) > 99){
            aux = arredondarParaCima(parseInt(array[1], 10));
            return "R$ " + array[0] + "," + aux;
        }

        return "R$ " + numeroParcialmenteFormatado;
    }else{
        let numeroParcialmenteFormatado =  Math.abs(value).toLocaleString('pt-BR');
        let array = numeroParcialmenteFormatado.split(",");
        let aux = 0;
    
        if (parseInt(array[1], 10) > 99){
            aux = arredondarParaCima(parseInt(array[1], 10));
            return "-R$ " + array[0] + "," + aux;
        }

    return "-R$ " + numeroParcialmenteFormatado;
    }
}

console.log(imprimirBRL(4.651));
console.log(imprimirBRL(0));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-3498.99));
console.log(imprimirBRL(2313477.0135));