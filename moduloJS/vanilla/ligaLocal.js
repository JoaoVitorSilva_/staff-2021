class Jogador {
    constructor (nome, numero) {
        this._nome = nome;
        this._numero = numero;
    }

    get nome () {
        return this._nome;
    }

    get numero () {
        return this._numero;
    }
}

class Time {
    constructor (nome, tipoEsporte, status, liga) {
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = status;
        this._liga = liga;
        this._jogadores = [];
    }

    get retornaNome() {
        return this._nome;
    }

    get retornaTipoEsporte() {
        return this._tipoEsporte;
    }

    get retornaStatus() {
        return this._status;
    }

    get retornaLiga() {
        return this._liga;
    }

    adicionarJogador(jogador) {
        this._jogadores.push(jogador);
    }

    buscarJogadorPorNome(nomeJogador) {
        return this._jogadores.filter( jogador => jogador._nome === nomeJogador);
    }

    buscarJogadorPorNumero(numeroJogador) {
        return this._jogadores.filter( jogador => jogador._numero === numeroJogador );
    }
}

let internacional = new Time ("Internacional", "Futebol", "Ativo", "Brasileirão")

let taison = new Jogador ("Taison", 10);
internacional.adicionarJogador(taison);

console.log(internacional);
console.log(internacional.buscarJogadorPorNome("Taison"));
console.log(internacional.buscarJogadorPorNumero(10));