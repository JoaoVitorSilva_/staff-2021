//import PropTypes from 'prop-types';

class ListaSeries {
	constructor(){
    
		this.series = JSON.parse(`[
      {
        "titulo": "Stranger Things",
        "anoEstreia": 2016,
        "diretor": [
          "Matt Duffer",
          "Ross Duffer"
        ],
        "genero": [
          "Suspense",
          "Ficcao Cientifica",
          "Drama"
        ],
        "elenco": [
          "Winona Ryder",
          "David Harbour",
          "Finn Wolfhard",
          "Millie Bobby Brown",
          "Gaten Matarazzo",
          "Caleb McLaughlin",
          "Natalia Dyer",
          "Charlie Heaton",
          "Cara Buono",
          "Matthew Modine",
          "Noah Schnapp"
        ],
        "temporadas": 2,
        "numeroEpisodios": 17,
        "distribuidora": "Netflix"
      },
      {
        "titulo": "Game Of Thrones",
        "anoEstreia": 2011,
        "diretor": [
          "David Benioff",
          "D. B. Weiss",
          "Carolyn Strauss",
          "Frank Doelger",
          "Bernadette Caulfield",
          "George R. R. Martin"
        ],
        "genero": [
          "Fantasia",
          "Drama"
        ],
        "elenco": [
          "Peter Dinklage",
          "Nikolaj Coster-Waldau",
          "Lena Headey",
          "Emilia Clarke",
          "Kit Harington",
          "Aidan Gillen",
          "Iain Glen ",
          "Sophie Turner",
          "Maisie Williams",
          "Alfie Allen",
          "Isaac Hempstead Wright"
        ],
        "temporadas": 7,
        "numeroEpisodios": 67,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The Walking Dead",
        "anoEstreia": 2010,
        "diretor": [
          "Jolly Dale",
          "Caleb Womble",
          "Paul Gadd",
          "Heather Bellson"
        ],
        "genero": [
          "Terror",
          "Suspense",
          "Apocalipse Zumbi"
        ],
        "elenco": [
          "Andrew Lincoln",
          "Jon Bernthal",
          "Sarah Wayne Callies",
          "Laurie Holden",
          "Jeffrey DeMunn",
          "Steven Yeun",
          "Chandler Riggs ",
          "Norman Reedus",
          "Lauren Cohan",
          "Danai Gurira",
          "Michael Rooker ",
          "David Morrissey"
        ],
        "temporadas": 9,
        "numeroEpisodios": 122,
        "distribuidora": "AMC"
      },
      {
        "titulo": "Band of Brothers",
        "anoEstreia": 20001,
        "diretor": [
          "Steven Spielberg",
          "Tom Hanks",
          "Preston Smith",
          "Erik Jendresen",
          "Stephen E. Ambrose"
        ],
        "genero": [
          "Guerra"
        ],
        "elenco": [
          "Damian Lewis",
          "Donnie Wahlberg",
          "Ron Livingston",
          "Matthew Settle",
          "Neal McDonough"
        ],
        "temporadas": 1,
        "numeroEpisodios": 10,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The JS Mirror",
        "anoEstreia": 2017,
        "diretor": [
          "Lisandro",
          "Jaime",
          "Edgar"
        ],
        "genero": [
          "Terror",
          "Caos",
          "JavaScript"
        ],
        "elenco": [
          "Amanda de Carli",
          "Alex Baptista",
          "Gilberto Junior",
          "Gustavo Gallarreta",
          "Henrique Klein",
          "Isaias Fernandes",
          "João Vitor da Silva Silveira",
          "Arthur Mattos",
          "Mario Pereira",
          "Matheus Scheffer",
          "Tiago Almeida",
          "Tiago Falcon Lopes"
        ],
        "temporadas": 5,
        "numeroEpisodios": 40,
        "distribuidora": "DBC"
      },
      {
        "titulo": "Mr. Robot",
        "anoEstreia": 2018,
        "diretor": [
          "Sam Esmail"
        ],
        "genero": [
          "Drama",
          "Techno Thriller",
          "Psychological Thriller"
        ],
        "elenco": [
          "Rami Malek",
          "Carly Chaikin",
          "Portia Doubleday",
          "Martin Wallström",
          "Christian Slater"
        ],
        "temporadas": 3,
        "numeroEpisodios": 32,
        "distribuidora": "USA Network"
      },
      {
        "titulo": "Narcos",
        "anoEstreia": 2015,
        "diretor": [
          "Paul Eckstein",
          "Mariano Carranco",
          "Tim King",
          "Lorenzo O Brien"
        ],
        "genero": [
          "Documentario",
          "Crime",
          "Drama"
        ],
        "elenco": [
          "Wagner Moura",
          "Boyd Holbrook",
          "Pedro Pascal",
          "Joann Christie",
          "Mauricie Compte",
          "André Mattos",
          "Roberto Urbina",
          "Diego Cataño",
          "Jorge A. Jiménez",
          "Paulina Gaitán",
          "Paulina Garcia"
        ],
        "temporadas": 3,
        "numeroEpisodios": 30,
        "distribuidora": null
      },
      {
        "titulo": "Westworld",
        "anoEstreia": 2016,
        "diretor": [
          "Athena Wickham"
        ],
        "genero": [
          "Ficcao Cientifica",
          "Drama",
          "Thriller",
          "Acao",
          "Aventura",
          "Faroeste"
        ],
        "elenco": [
          "Anthony I. Hopkins",
          "Thandie N. Newton",
          "Jeffrey S. Wright",
          "James T. Marsden",
          "Ben I. Barnes",
          "Ingrid N. Bolso Berdal",
          "Clifton T. Collins Jr.",
          "Luke O. Hemsworth"
        ],
        "temporadas": 2,
        "numeroEpisodios": 20,
        "distribuidora": "HBO"
      },
      {
        "titulo": "Breaking Bad",
        "anoEstreia": 2008,
        "diretor": [
          "Vince Gilligan",
          "Michelle MacLaren",
          "Adam Bernstein",
          "Colin Bucksey",
          "Michael Slovis",
          "Peter Gould"
        ],
        "genero": [
          "Acao",
          "Suspense",
          "Drama",
          "Crime",
          "Humor Negro"
        ],
        "elenco": [
          "Bryan Cranston",
          "Anna Gunn",
          "Aaron Paul",
          "Dean Norris",
          "Betsy Brandt",
          "RJ Mitte"
        ],
        "temporadas": 5,
        "numeroEpisodios": 62,
        "distribuidora": "AMC"
      }
    ]`);

    console.log(this.series)
   
  }

// ATIVIDADE 1
  invalidas = () => {
    let ano = new Date();
        ano = ano.getFullYear();
        let seriesInvalidas = 'Series invalidas:'
        let titulos = this.series.map( serie => {
            let seNaoForValido = false;
            Object.keys( serie ).forEach( function ( item ) {
                if ( serie[ item ] === null )
                seNaoForValido = true
            } );
            if ( ( serie.anoEstreia > ano || seNaoForValido ) ) {
                return ` ${serie.titulo} -`;
            }
        } );
        return seriesInvalidas + ( titulos.join( '' ).replace(/-$/, '') );
        
  }

//ATIVIDADE 2
  filtrarPorAno = ano => {
    let seriesAnoEspecifico = this.series.filter( serie => serie.anoEstreia >= ano );
    return seriesAnoEspecifico;
  }

//ATIVIDADE 3
  procurarPorNome = nome => {
    let resultado = false;
    this.series.forEach( serie => {
      let validacao = serie.elenco.includes(nome);
      
      if(validacao){
        resultado = true;
      }
    })
    return resultado;
  }

//ATIVIDADE 4
  mediaDeEpisodios = () => {
    let totalDeEpisodios = 0;
    const quantidade = this.series.length;
    this.series.map( serie => totalDeEpisodios += serie.numeroEpisodios );
    return totalDeEpisodios / quantidade;
  }

//ATIVIDADE 5
  totalSalarios = posicao => {
    const salarioDiretor = 100.000;
    const salarioOperarios = 40.000;
    const qtdDiretores = this.series[posicao].diretor.length;
    const qtdElenco = this.series[posicao].elenco.length;
    return ( salarioDiretor * qtdDiretores ) + ( salarioOperarios * qtdElenco );
  }

//ATIVIDADE 6A  
  queroGenero = ( genero ) => {
    let titulos = [];
    this.series.filter( serie => {
      let validacao = serie.genero.includes( genero );
      if( validacao ) {
        titulos.push(serie.titulo);
      }
    } )
    return titulos;
  }

//ATIVIDADE 6B
  queroTitulo = ( titulo ) => {
    let titulos = [];
    this.series.filter( serie => {
      let validacao = serie.titulo.includes( titulo );
      if( validacao ) {
        titulos.push(serie.titulo);
      }
    } )
    return titulos;
  }

//ATIVIDADE 7
  creditos = ( posicao ) => {
    const titulo = this.series[posicao].titulo;
    
    let diretores = this.series[posicao].diretor.map( diretor => diretor.split( ' ' ).reverse().join( ' ' ) );
    diretores.sort();
    diretores = diretores.map( diretor => diretor.split(' ').reverse().join(' ') )
    
    let elenco = this.series[posicao].elenco.map( elenco => elenco.split( ' ' ).reverse().join( ' ' ) );
    elenco.sort();
    elenco = elenco.map( elenco => elenco.split(' ').reverse().join(' ') )
    return { titulo, diretores, elenco };
  }

//ATIVIDADE 8
  verificarAbreviacao = nome => nome.includes('. ');

  hashTag = () => {
    let hash = '#';
    this.series.forEach(serie => {
      let verificacao = serie.elenco.map( ator => this.verificarAbreviacao( ator ) );

      if( !verificacao.includes( false ) ){
        serie.elenco.map( ator => {
          let posicao = ator.indexOf( '.' ) - 1;
          hash += ator.substr( posicao, 1 );
        } )
      }
    } );
    return hash;
  }

}
/*
ListaSeries.propTypes = {
  invalidas: PropTypes.array,
  filtrarPorAno: PropTypes.array,
  procurarPorNome: PropTypes.array,
  mediaDeEpisodios: PropTypes.array,
  totalSalarios: PropTypes.array,
  queroGenero: PropTypes.array,
  queroTitulo: PropTypes.array,
  creditos: PropTypes.array,
  hashTag: PropTypes.array
}
*/

let series = new ListaSeries();
console.log('ATIVIDADE 1')
console.log(series.invalidas())
console.log('ATIVIDADE 2')
console.log(series.filtrarPorAno(2017))
console.log('ATIVIDADE 3')
console.log(series.procurarPorNome('João Pacheco'))
console.log('ATIVIDADE 3 - TRUE')
console.log(series.procurarPorNome('Anna Gunn'))
console.log('ATIVIDADE 4')
console.log(series.mediaDeEpisodios())
console.log('ATIVIDADE 5')
console.log(series.totalSalarios(0))
console.log('ATIVIDADE 6A')
console.log(series.queroGenero('Caos'))
console.log('ATIVIDADE 6B')
console.log(series.queroTitulo('The'))
console.log('ATIVIDADE 7')
console.log(series.creditos(5))
console.log('ATIVIDADE 8')
console.log(series.hashTag())