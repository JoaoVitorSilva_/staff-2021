import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import RotaPrivada from '../Components/RotaPrivada';
import Login from './Login';

import App from './App';
import CadastroCliente from './CadastroCliente';
import CadastroEspaco from './CadastroEspaco';
import CadastroPacote from './CadastroPacote';


export default class Rotas extends Component {
  render() {
    return (
      <Router>
        <Route path="/login" exact component={ Login } />
        <RotaPrivada path="/" exact component={ App } />
        <RotaPrivada path="/cadastroCliente" exact component={ CadastroCliente } />
        <RotaPrivada path="/cadastroEspaco" exact component={ CadastroEspaco } />
        <RotaPrivada path="/cadastroPacote" exact component={ CadastroPacote } />
      </Router>
    );
  }
}
