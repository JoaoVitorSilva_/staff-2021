import React, { Component } from 'react';

import FormCadastroClienteUI from '../../Components/FormCadastroClienteUI';
import MenuIU from '../../Components/MenuUI/index';
import HeaderUI from '../../Components/HeaderUI';
import BreadcrumbCadClienteUI from '../../Components/BreadcrumbleCadClienteUI';

import Layout from '../../Middler/LayoutMiddler'
import Content from '../../Middler/ContentMiddler'
import Col from '../../Middler/ColMiddler'
import Row from '../../Middler/RowMiddler'

import CoworkingBackAPI from '../../Models/CoworkingBackAPI';
import ClienteReformed from '../../Models/ClienteReformed';

import '../App.css'

const { Footer, Sider } = Layout;

export default class CadastroCliente extends Component {
  constructor(props) {
    super(props)
    this.coworkingBackAPI = new CoworkingBackAPI();
  }

  cadastrarCliente(cliente) {
    let clienteCriado = new ClienteReformed(cliente);
    this.coworkingBackAPI.cadastrarCliente(clienteCriado);
  }

  render() {

    return (
      <React.Fragment>
        <Layout>
          <HeaderUI>
          </HeaderUI>
          <Layout>
            <Sider>
              <MenuIU />
            </Sider>
            <Layout>
              <Content style={{ padding: '0 30px' }}>
                <BreadcrumbCadClienteUI></BreadcrumbCadClienteUI>
                <Row>
                  <Col span={5}></Col>
                  <Col style={ { margin: '60px', textAlign: 'center'  } } span={10}>
                    <FormCadastroClienteUI
                      metodoCadastrar={this.cadastrarCliente.bind(this)}
                    />
                  </Col>
                  <Col span={8}></Col>
                </Row>
              </Content>
              <Footer style={{ textAlign: 'center' }}>João Vitor ©2021 - Criado por João Vitor</Footer>
            </Layout>
          </Layout>
        </Layout>
      </React.Fragment >
    );
  }
}