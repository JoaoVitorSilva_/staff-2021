import React, { Component } from 'react';
import { isAuthenticated } from './Authentication';

import FormLoginUI from '../../Components/FormLoginUI';
import MenuIU from '../../Components/MenuUI/index';
import HeaderUI from '../../Components/HeaderUI';
import BreadcrumbLoginUI from '../../Components/BreadcrumbLoginUI';

import Layout from '../../Middler/LayoutMiddler'
import Content from '../../Middler/ContentMiddler'
import Col from '../../Middler/ColMiddler'
import Row from '../../Middler/RowMiddler'

import CoworkingBackAPI from '../../Models/CoworkingBackAPI';

import '../App.css'

const { Footer, Sider } = Layout;

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.coworkingBackAPI = new CoworkingBackAPI();
  }

  realizarLogin( dados ) {
    console.log( dados ) 
    this.coworkingBackAPI.realizarLogin( dados )
      .then(() => {
        if (isAuthenticated()) {
          this.props.history.push('/');
        }
      })
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          <HeaderUI>
          </HeaderUI>
          <Layout>
            <Sider>
              <MenuIU />
            </Sider>
            <Layout>
              <Content style={{ padding: '0 30px' }}>
              <BreadcrumbLoginUI></BreadcrumbLoginUI>
                <Row>
                  <Col span={3}></Col>
                  <Col style={ { margin: '140px', textAlign: 'center'  } } span={10}>
                    <FormLoginUI
                      metodoCadastrar={this.realizarLogin.bind(this)}
                    />
                  </Col>
                  <Col span={7}></Col>
                </Row>
              </Content>
              <Footer style={{ textAlign: 'center' }}>João Vitor ©2021 - Criado por João Vitor</Footer>
            </Layout>
          </Layout>
        </Layout>
      </React.Fragment >
    );
  }
}