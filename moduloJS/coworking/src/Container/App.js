import React, { Component } from 'react';
import './App.css';
import { Layout, Carousel, Image } from 'antd';

import MenuIU from '../Components/MenuUI/index';
import HeaderUI from '../Components/HeaderUI';
import BreadcrumbUI from '../Components/BreadcrumbUI';

const { Footer, Sider, Content } = Layout;

export default class App extends Component {

  render() {
    return (
      <Layout>
        <HeaderUI>
        </HeaderUI>
        <Layout>
          <Sider>
            <MenuIU />
          </Sider>
          <Layout>
            <Content style={{ padding: '0 30px' }}>
              <BreadcrumbUI></BreadcrumbUI>
              <Carousel autoplay>
                <div>
                  <Image src='https://i.ytimg.com/vi/NU8tboDmhV4/maxresdefault.jpg' className='carrosel'></Image>
                </div>
                <div>
                  <Image src='https://www.agoratechpark.com.br/wp-content/uploads/cow-working-no-a%CC%81gora.jpg' className='carrosel'></Image>
                </div>
                <div>
                  <Image src='https://mid.curitiba.pr.gov.br/2018/capa/00238323.jpg' className='carrosel'></Image>
                </div>
                <div>
                  <Image src='https://exame.com/wp-content/uploads/2017/08/coworking-devonshire-square.jpg' className='carrosel'></Image>
                </div>

              </Carousel>,
            </Content>
            <Footer style={{ textAlign: 'center' }}>João Vitor ©2021 - Criado por João Vitor</Footer>
          </Layout>
        </Layout>
      </Layout>
    )
  }
}