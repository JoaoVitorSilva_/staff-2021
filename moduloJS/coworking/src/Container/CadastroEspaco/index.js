import React, { Component } from 'react';

import FormCadastroEspacoUI from '../../Components/FormCadastroEspacoUI';
import MenuIU from '../../Components/MenuUI/index';
import HeaderUI from '../../Components/HeaderUI';
import BreadcrumbCadEspacoUI from '../../Components/BreadcrumbleCadEspacoUI';

import Layout from '../../Middler/LayoutMiddler'
import Content from '../../Middler/ContentMiddler'
import Col from '../../Middler/ColMiddler'
import Row from '../../Middler/RowMiddler'

import CoworkingBackAPI from '../../Models/CoworkingBackAPI';

import '../App.css'

const { Footer, Sider } = Layout;

export default class CadastroEspaco extends Component {
  constructor( props ) {
    super( props )
    this.coworkingBackAPI = new CoworkingBackAPI();
  }

  cadastrarEspaco( espaco ) {
    this.coworkingBackAPI.cadastrarEspaco(espaco);
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          <HeaderUI>
          </HeaderUI>
          <Layout>
            <Sider>
              <MenuIU />
            </Sider>
            <Layout>
              <Content style={{ padding: '0 30px' }}>
              <BreadcrumbCadEspacoUI></BreadcrumbCadEspacoUI>
                <Row>
                  <Col span={4}></Col>
                  <Col style={ { margin: '120px', textAlign: 'center'  } } span={10}>
                    <FormCadastroEspacoUI
                      metodoCadastrar={this.cadastrarEspaco.bind(this)}
                    />
                  </Col>
                  <Col span={8}></Col>
                </Row>
              </Content>
              <Footer style={{ textAlign: 'center' }}>João Vitor ©2021 - Criado por João Vitor</Footer>
            </Layout>
          </Layout>
        </Layout>
      </React.Fragment >
    );
  }
}