import React from 'react';
import { Breadcrumb } from 'antd';

const BreadcrumbCadEspacoUI = ({ }) =>
  <Breadcrumb className='centralizar' style={{ margin: '16px 0' }}>
    <Breadcrumb.Item className='ant-breadcrumb-link' >Cadastro dos Espaços</Breadcrumb.Item>
  </Breadcrumb>

export default BreadcrumbCadEspacoUI;