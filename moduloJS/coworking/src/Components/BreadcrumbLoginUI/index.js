import React from 'react';
import { Breadcrumb } from 'antd';

const BreadcrumbLoginUI = ({ }) =>
  <Breadcrumb className='centralizar' style={{ margin: '16px 0' }}>
    <Breadcrumb.Item className='ant-breadcrumb-link' >POR FAVOR REALIZE O SEU LOGIN</Breadcrumb.Item>
  </Breadcrumb>

export default BreadcrumbLoginUI;