import React from 'react';
import { Link } from 'react-router-dom';
import Menu from '../../Middler/MenuMiddler'

const MenuItemUI = ({ metodo, nome, link, icon }) => 
  <React.Fragment>
    <Menu.Item type='primary' onClick={ metodo } >
      { link ? <Link to={ link } >{ icon }{ nome }</Link> : nome }
    </Menu.Item>
  </React.Fragment>

export default MenuItemUI;