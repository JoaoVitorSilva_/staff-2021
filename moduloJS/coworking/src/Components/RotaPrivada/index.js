import { Redirect, Route } from 'react-router-dom';
import { isAuthenticated } from '../../Container/Login/Authentication';

export default function RotaPrivada({ component: Component, ...rest } ) {
  return (
    <Route 
      { ...rest } 
      render={ props => (
        isAuthenticated() ?
        <Component { ...props } /> :
        <Redirect to={ { pathname: '/login', state: { from: props.location } } } />
        ) 
      } 
    />
  );
}