import React from 'react';
import { Breadcrumb } from 'antd';

const BreadcrumbCadPacoteUI = ({ }) =>
  <Breadcrumb className='centralizar' style={{ margin: '16px 0' }}>
    <Breadcrumb.Item className='ant-breadcrumb-link' >Cadastro dos Pacotes</Breadcrumb.Item>
  </Breadcrumb>

export default BreadcrumbCadPacoteUI;