import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';

import Form from '../../Middler/FormMiddler';
import Input from '../../Middler/InputMiddler';
import ButtonSubmitUI from '../ButtonSubmitUI';

import '../../Container/App.css'

const FormLoginUI = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    console.log(values)
    metodoCadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <Form onFinish={onFinish}>
          <Form.Item label="Login" name="username">
            <Input />
          </Form.Item>
          <Form.Item label="Senha" name="password">
            <Input />
          </Form.Item>
          <Form.Item className="BotaoParaCadastrar">
            <ButtonSubmitUI
              nome={"Login"}
            />
          </Form.Item>
        </Form>
      </React.Fragment >)
      :
      (<Redirect to='/' />)
  )

}

export default FormLoginUI;