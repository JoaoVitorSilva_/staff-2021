import React from 'react';

import { Link } from 'react-router-dom';

import Button from '../../Middler/ButtonMiddler'

const ButtonSubmitUI = ({ metodo, nome, link }) => 
  <React.Fragment>
    <Button type="primary" htmlType="submit" >
      { link ? <Link to={ link }> { nome } </Link> : nome }
    </Button>
  </React.Fragment>

export default ButtonSubmitUI;