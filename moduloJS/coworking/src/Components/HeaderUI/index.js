import React from 'react';
import Header from '../../Middler/HeaderMiddler';
import Title from 'antd/lib/typography/Title';
import AvatarUI from '../AvatarUI';
import { UserOutlined } from '@ant-design/icons';

const HeaderUI = ( { } ) =>
<Header style={ { padding:15 }}>
  <AvatarUI link={'/cadastroCliente'} icon={<UserOutlined/>} />
  <Title  style={ { color:'white' } } level={3}>CoworkingJoão</Title>
</Header>

export default HeaderUI;