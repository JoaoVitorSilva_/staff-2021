import React from 'react';

import Form from '../../Middler/FormMiddler';
import Input from '../../Middler/InputMiddler';
import ButtonSubmitUI from '../ButtonSubmitUI';
import ButtonUI from '../ButtonUI';

import '../../Container/App.css'


const FormCadastroEspacoUI = ({ metodoCadastrar }) => {

  const onFinish = (values) => {
    metodoCadastrar(values);
  };

  return (
    <React.Fragment>
      <Form onFinish={onFinish}>
        <Form.Item label="Nome" name="nome">
          <Input />
        </Form.Item>
        <Form.Item label="Quantidade de pessoas" name="qtdPessoas">
          <Input />
        </Form.Item>
        <Form.Item label="Valor" name="valor">
          <Input />
        </Form.Item>
        <ButtonUI
          nome={"Voltar"}
          link={"/"}
        />
        <Form.Item className="BotaoParaCadastrar">
          <ButtonSubmitUI
            nome={"Cadastrar"}
            link={"/"}
          />
        </Form.Item>
      </Form>
    </React.Fragment>
  )
}

export default FormCadastroEspacoUI;