import React from 'react';
import { Link } from 'react-router-dom';
import Avatar from '../../Middler/AvatarMiddler'


const AvatarUI = ({ metodo, link, icon }) =>
  <React.Fragment>
    <Avatar type='primary' onClick={ metodo } style={{ float: 'right' }}>
    { link ? <Link to={ link } >{ icon }</Link> : icon }
    </Avatar>
  </React.Fragment>

export default AvatarUI