import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../../Middler/ButtonMiddler'

const ButtonUI = ({ metodo, nome, link }) => 
  <React.Fragment>
    <Button type="primary" onClick={ metodo } >
      { link ? <Link to={ link } >{ nome }</Link> : nome }
    </Button>
  </React.Fragment>

export default ButtonUI;