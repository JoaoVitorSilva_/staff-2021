import React from 'react';

import Form from '../../Middler/FormMiddler';
import Input from '../../Middler/InputMiddler';
import ButtonSubmitUI from '../ButtonSubmitUI';
import ButtonUI from '../ButtonUI';
import DatePicker from '../../Middler/DatePickerMiddler';

import '../../Container/App.css';

const FormCadastroClienteUI = ({ metodoCadastrar }) => {

  const onFinish = (values) => {
    metodoCadastrar(values);
  };

  return (
    <React.Fragment>
      <Form onFinish={onFinish}>
        <Form.Item label="Nome" name="nome">
          <Input />
        </Form.Item>
        <Form.Item label="CPF" name="cpf">
          <Input />
        </Form.Item>
        <Form.Item label="Email" name="email">
          <Input />
        </Form.Item>
        <Form.Item label="Telefone" name="telefone">
          <Input />
        </Form.Item>
        <Form.Item label="Data de nascimento" name="dataNascimento">
          <DatePicker />
        </Form.Item>
        <ButtonUI
          nome={"Voltar"}
          link={"/"}
        />
        <Form.Item className="BotaoParaCadastrar">
          <ButtonSubmitUI
            nome={"Cadastrar"}
            link={"/"}
          />
        </Form.Item>
      </Form>
    </React.Fragment>
  )
}

export default FormCadastroClienteUI;