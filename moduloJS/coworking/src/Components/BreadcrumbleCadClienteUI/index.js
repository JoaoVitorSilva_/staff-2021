import React from 'react';
import { Breadcrumb } from 'antd';

const BreadcrumbCadClienteUI = ({ }) =>
  <Breadcrumb className='centralizar' style={{ margin: '16px 0' }}>
    <Breadcrumb.Item className='ant-breadcrumb-link' >Cadastro de Clientes</Breadcrumb.Item>
  </Breadcrumb>

export default BreadcrumbCadClienteUI;