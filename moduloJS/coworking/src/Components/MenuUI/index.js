import React from 'react';
import { BankFilled, HomeFilled, FolderOpenFilled, UsergroupAddOutlined } from '@ant-design/icons';
import SubMenu from 'antd/lib/menu/SubMenu';
import Menu from '../../Middler/MenuMiddler'
import MenuItemUI from '../MenuItemUI';

const MenuIU = ({ Link }) =>
<Menu defaultSelectedKeys={['paginaInicial']} mode='inline'>
  <MenuItemUI link={'/'} nome={'Pagina Inicial'} icon={ <HomeFilled /> }> Página Inicial </MenuItemUI>

  <SubMenu key="sub1" icon={< UsergroupAddOutlined />} title="Área do Cliente">
      <MenuItemUI link={'/cadastroCliente'} nome={'Cadastro de Clientes'}></MenuItemUI>
  </SubMenu>

  <SubMenu key="sub3" icon={< BankFilled />} title="Área dos Espaços">
      <MenuItemUI link={'/cadastroEspaco'} nome={'Cadastro de Espaços'}></MenuItemUI>
  </SubMenu>

  <SubMenu key="sub2" icon={< FolderOpenFilled />} title="Área dos Pacotes">
      <MenuItemUI link={'/cadastroPacote'} nome={'Cadastro de Pacotes'}></MenuItemUI>
  </SubMenu>
</Menu>

export default MenuIU;
