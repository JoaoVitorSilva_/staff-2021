import React from 'react';
import { Breadcrumb } from 'antd';

const BreadcrumbUI = ({ }) =>
  <Breadcrumb className='centralizar' style={{ margin: '16px 0' }}>
    <Breadcrumb.Item className='ant-breadcrumb-link' >BEM VINDO AO MEU PROJETO COWORKING</Breadcrumb.Item>
    <Breadcrumb.Item className='ant-breadcrumb-link' >Utilize os botões ao lado para se localizar no site</Breadcrumb.Item>
  </Breadcrumb>

export default BreadcrumbUI;