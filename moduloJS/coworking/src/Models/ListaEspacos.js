import Espaco from "./Espaco";

export default class ListaEspaco {
  constructor( arrayEspaco ) {
    this.espacos = arrayEspaco.map( espaco => new Espaco( espaco ) );
  }

}