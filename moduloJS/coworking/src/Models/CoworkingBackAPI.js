import axios from 'axios';
import { getToken } from '../Container/Login/Authentication';
import { login } from '../Container/Login/Authentication';

const api = axios.create({
  baseURL: "http://localhost:8080"
});

api.interceptors.request.use(async config => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default class CoworkingBackAPI {

  async realizarLogin({ username, password }) {
    const response = await api.post( "/login", { username, password } );
    try{
      const headerAuthorization = response.headers.authorization.split(' ');
      const token = headerAuthorization[1];
      login(token);
    } catch( err ) {
      console.log(`Não foi possível fazer a autenticação.`);
    }
  }

  cadastrarPacote( pacote ) {
    return api.post( `api/pacotes/salvar`, pacote );
  }

  cadastrarEspaco( espaco ) {
    return api.post( `api/espaco/salvar`, espaco );
  }

  cadastrarCliente( cliente ) {
    return api.post( `api/cliente/salvar`, cliente );
  }
}