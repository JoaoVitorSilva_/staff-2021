import Pacote from "./Pacote";

export default class ListaPacotes {
  constructor( arrayPacotes ) {
    this.pacotes = arrayPacotes.map( pacote => new Pacote( pacote ) );
  }

}