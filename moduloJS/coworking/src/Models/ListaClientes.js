import Cliente from "./Cliente";

export default class ListaClientes {
  constructor( arrayClientes ) {
    this.clientes = arrayClientes.map( cliente => new Cliente( cliente ) );
  }

}