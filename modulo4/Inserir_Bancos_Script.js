db.createCollection("banco")

var enderecoDefault = {
    logradouro: "Testando",
    numero: "1",
    complemento: "",
    cep: "90000000",
    bairro: "NA",
    cidade: "NA",
    estado: "NA",
    pais: "Brasil"
}

var bancoAlfa = {
        codigo: "011",
        nome: "Alfa",
        agencia: [
            {
                codigo: "0001",
                nome: "Web",
                endereco: {
                    logradouro: "Rua Testando",
                    numero: "55",
                    complemento: "loja 1",
                    cep: "900000000",
                    bairro: "NA",
                    cidade: "NA",
                    estado: "NA",
                    pais: "Brasil"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "1",
                        tipoConta: "PJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Cleber",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Manuel Neuer",
                                CPF: "123.254.254-01",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Lauro",
                                CPF: "500.600.700-08",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "555.652.154-03",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "2",
                        tipoConta: "PJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Cleber",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Manuel Neuer",
                                CPF: "123.254.254-01",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Lauro",
                                CPF: "500.600.700-08",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "555.652.154-03",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "3",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Lucas",
                                CPF: "222.222.222-02",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Emily",
                                CPF: "111.111.111-01",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Marcelo",
                                CPF: "485.145.231-04",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "4",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Enricco",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fernando",
                                CPF: "333.333.333-03",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Denise",
                                CPF: "485.111.658-03",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    }
                ]
            },
            {
                codigo: "0002",
                nome: "California",
                endereco: {
                    logradouro: "Testing",
                    numero: "122",
                    complemento: "",
                    cep: "90000000",
                    bairro: "Between Hyde and Powell Streets",
                    cidade: "San Francisco",
                    estado: "California",
                    pais: "England"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "5",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Ketlin",
                                CPF: "454.544.555-44",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "6",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "7",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    }
                ]
            },
            {
                codigo: "0101",
                nome: "Londres",
                endereco: {
                    logradouro: "Testing",
                    numero: "525",
                    complemento: "",
                    cep: "90000000",
                    bairro: "Croydon",
                    cidade: "Londres",
                    estado: "Boroughs",
                    pais: "England"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "8",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "9",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "10",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                ]
            }
        ]
    }
    
db.banco.insert(
    {
        codigo: "011",
        nome: "Alfa",
        agencia: [
            {
                codigo: "0001",
                nome: "Web",
                endereco: {
                    logradouro: "Rua Testando",
                    numero: "55",
                    complemento: "loja 1",
                    cep: "900000000",
                    bairro: "NA",
                    cidade: "NA",
                    estado: "NA",
                    pais: "Brasil"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "1",
                        tipoConta: "PJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Julio Cezar",
                                CPF: "63465457013",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "2",
                        tipoConta: "PJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GG",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Julio Cezar",
                                CPF: "63465457013",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "3",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Julio Cezar",
                                CPF: "63465457013",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "4",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Julio Cezar",
                                CPF: "63465457013",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    }
                ]
            },
            {
                codigo: "0002",
                nome: "California",
                endereco: {
                    logradouro: "Testing",
                    numero: "122",
                    complemento: "",
                    cep: "90000000",
                    bairro: "Between Hyde and Powell Streets",
                    cidade: "San Francisco",
                    estado: "California",
                    pais: "England"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "5",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "6",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "7",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    }
                ]
            },
            {
                codigo: "0101",
                nome: "Londres",
                endereco: {
                    logradouro: "Testing",
                    numero: "525",
                    complemento: "",
                    cep: "90000000",
                    bairro: "Croydon",
                    cidade: "Londres",
                    estado: "Boroughs",
                    pais: "England"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "8",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "9",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "10",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                ]
            }
        ]
    }
);

db.banco.insert(
    {
        codigo: "241",
        nome: "Beta",
        agencia: [
            {
                codigo: "0001",
                nome: "Web",
                endereco: {
                    logradouro: "Testando",
                    numero: "55",
                    complemento: "loja 2",
                    cep: "90000000",
                    bairro: "NA",
                    cidade: "NA",
                    estado: "NA",
                    pais: "Brasil"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "11",
                        tipoConta: "PJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "12",
                        tipoConta: "PJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "13",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "14",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "15",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "16",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "17",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "18",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "19",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "20",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }
);

db.banco.insert(
    {
        codigo: "307",
        nome: "Omega",
        agencia: [
            {
                codigo: "0001",
                nome: "Web",
                endereco: {
                    logradouro: "Testando",
                    numero: "55",
                    complemento: "loja 3",
                    cep: "90000000",
                    bairro: "NA",
                    cidade: "NA",
                    estado: "NA",
                    pais: "Brasil"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "21",
                        tipoConta: "PJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Julio Cezar",
                                CPF: "63465457013",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "22",
                        tipoConta: "PJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GG",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Julio Cezar",
                                CPF: "63465457013",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "23",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Julio Cezar",
                                CPF: "63465457013",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "24",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Julio Cezar",
                                CPF: "63465457013",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    }
                ]
            },
            {
                codigo: "8761",
                nome: "Itu",
                endereco: {
                    logradouro: "do Meio",
                    numero: "2233",
                    complemento: "",
                    cep: "90000000",
                    bairro: "Qualquer",
                    cidade: "Itu",
                    estado: "São Paulo",
                    pais: "Brasil"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "25",
                        tipoConta: "CONJ",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "26",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "27",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    }
                ]
            },
            {
                codigo: "4567",
                nome: "Hermana",
                endereco: {
                    logradouro: "do Boca",
                    numero: "222",
                    complemento: "",
                    cep: "90000000",
                    bairro: "Caminto",
                    cidade: "Buenos Aires",
                    estado: "Buenos Aires",
                    pais: "Argentina"
                },
                consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                },
                contas: [
                    {
                        codigo: "28",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            },
                            {
                                nome: "João",
                                CPF: "03445988021",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "29",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                    {
                        codigo: "30",
                        tipoConta: "PF",
                        saldo: 0.00,
                        gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ],
                        clientes: [
                            {
                                nome: "Fabiano",
                                CPF: "77152072094",
                                estadoCivil: "Solteiro",
                                dataDeNascimento: "2001-01-01",
                                endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: "NA",
                                    cidade: "NA",
                                    estado: "NA",
                                    pais: "Brasil"
                                }
                            }
                        ]
                    },
                ]
            }
        ]
    }
);

db.banco.find().pretty()