db.createCollection("pais");
db.pais.insert({ pais: "Brasil" });

db.createCollection("estado");
db.estado.insert({ estado: "Rio Grande do Sul"});

db.createCollection("cidade");
db.cidade.insert({ cidade: "Porto Alegre" });


db.createCollection("bairro");
db.bairro.insert({ bairro: "Anchieta" });


db.createCollection("endereco");

db.endereco.insert(
                    {  endereco: {
                                    logradouro: "Testando",
                                    numero: "1",
                                    complemento: "",
                                    cep: "90000000",
                                    bairro: ObjectId("60bfb2cb77c28cc4d5b83a9c"),
                                    cidade: ObjectId("60bfb2b777c28cc4d5b83a9b"),
                                    estado: ObjectId("60bfb24477c28cc4d5b83a98"),
                                    pais: ObjectId("60bfb24477c28cc4d5b83a97")
                                    }  
                    }                               
                    );
              
              
db.createCollection("pessoas");
db.pessoas.insert({ gerentes: [
                            {
                                codigoFuncionario: "01",
                                tipoGerente: "GC",
                                nome: "Anderson",
                                CPF: "72663153010",
                                estadoCivil: "casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: ObjectId("60bfb47c77c28cc4d5b83a9f")
                            } 
                            ],
                            
                    clientes: [
                            {
                                nome: "Manuel Neuer",
                                CPF: "123.254.254-01",
                                estadoCivil: "Casado",
                                dataDeNascimento: "2001-01-01",
                                endereco: ObjectId("60bfb47c77c28cc4d5b83a9f")
                            } 
                            ]
                        }
                    )
                    

db.createCollection("contas");
db.contas.insert({ contas: [
                    {
                        codigo: "1",
                        tipoConta: "PJ",
                        saldo: 0.00,
                        gerentes: ObjectId("60bfb5fe77c28cc4d5b83aa0"),
                        clientes: ObjectId("60bfb5fe77c28cc4d5b83aa0")
                        }
                        ]
                    }
                    )
                    
db.createCollection("consolidacao");
db.consolidacao.insert({
                    consolidacao: {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                }
            }
        )
              
db.createCollection("agencia");
db.agencia.insert({ agencia: [
            {
                codigo: "0001",
                nome: "Web",
                endereco: ObjectId("60bfb47c77c28cc4d5b83a9f"),
                condolidacao: ObjectId("60bfb94477c28cc4d5b83aa2"),
                gerentes: ObjectId("60bfb5fe77c28cc4d5b83aa0"),
                clientes: ObjectId("60bfb5fe77c28cc4d5b83aa0")
            }
            ]
}
)     

db.createCollection("bancos")
db.bancos.insert({
        bancoAlfa = {
        codigo: "011",
        nome: "Alfa",
        agencia: ObjectId("60bfba7777c28cc4d5b83aa3")
        }
    }
)
                        
                    

                        


