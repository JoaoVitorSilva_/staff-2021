public class Elfo extends Personagem {
    private int indiceFlecha;
    private static int qtdElfos;
    
   {
        this.indiceFlecha = 1;
        Elfo.qtdElfos = 0;
   }
    
    public Elfo(String nome){
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item(1, "Arco" ));
        this.inventario.adicionar(new Item(2, "Flecha" ));
        Elfo.qtdElfos++;
    }
    
    public static int getQuantidade() {
        return Elfo.qtdElfos;
    }
    
    public void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    protected int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }
    
    protected boolean possuiFlecha() {
        return this.getQtdFlecha() > 0;
    }
    
    protected void atirarFlecha(Dwarf dwarf){
        if (this.possuiFlecha()){
        this.getFlecha().setQuantidade(this.getQtdFlecha() - 1);
        this.aumentarXP();
        super.tomaDano();
        dwarf.tomaDano();  
    }
   } 
   
   public String imprimirNomeClasse() {
       return "Elfo";     
    }
}