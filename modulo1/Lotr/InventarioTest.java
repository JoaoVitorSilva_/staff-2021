import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest {
    
    /*@Test
    public void criarInventarioSemPassarValor(){
        Inventario inventario = new Inventario();
        assertEquals( 99, inventario.getItens().size() );
    }
    
    @Test
    public void criarInventarioPassandoValor(){
        Inventario inventario = new Inventario(40);
        assertEquals( 40, inventario.getItens().length );
    }*/
    
    @Test
    public void adicionarUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item( 1, "Espada de aço" );
        inventario.adicionar(item);
        assertEquals( item, inventario.getItens().get(0) );
    }
    
    @Test
    public void adicionarDoisItensInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item( 1, "Espada de aço" );
        Item item2 = new Item( 3, "Flecha de Luz" );
        inventario.adicionar(item);
        inventario.adicionar(item2);
        assertEquals( item, inventario.getItens().get(0) );
        assertEquals( item2, inventario.getItens().get(1) );
    }
    
    @Test
    public void obterUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item( 1, "Espada de aço" );
        inventario.adicionar(item);
        assertEquals( item, inventario.obter(0) );
    }
    
    @Test
    public void removerUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item( 1, "Espada de aço" );
        inventario.adicionar(item);
        inventario.remover(0);
        assertNull( inventario.obter(0) );
    }
    
    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        Item escudo = new Item( 2, "Escudo" );
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoesItens();
        assertEquals( "Espada,Escudo", resultado );
    }
    
    @Test
    public void getItemMaiorQuantidade() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        Item lanca = new Item( 5, "Lança" );
        Item escudo = new Item( 2, "Escudo" );
        inventario.adicionar(espada);
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        Item maiorQtd = inventario.getItemComMaiorQuantidade();
        assertEquals( lanca, maiorQtd );
    }
    
    @Test
    public void getItemMaiorQuantidadeComItensDeMesmaQuantidade() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        Item lanca = new Item( 5, "Lança" );
        Item escudo = new Item( 5, "Escudo" );
        inventario.adicionar(espada);
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        Item maiorQtd = inventario.getItemComMaiorQuantidade();
        assertEquals( lanca, maiorQtd );
    }
    
    @Test
    public void buscarApenasUmItem() {
        Inventario inventario = new Inventario();
        Item cafe = new Item( 1, "Cafe" );
        inventario.adicionar(cafe);
        Item resultado = inventario.buscar(new String( "Cafe" ));
        assertEquals( cafe, resultado );
    }
    
    @Test
    public void inverterDoisItens() {
        Inventario inventario = new Inventario();
        Item termica = new Item( 1, "Termica de Café" );
        Item energetico = new Item( 1, "Energetico" );
        inventario.adicionar( termica );
        inventario.adicionar( energetico );
        ArrayList<Item> resultado = inventario.inverter();
        
        assertEquals( energetico, resultado.get(0) );
        assertEquals( termica, resultado.get(1) );
        
        assertEquals( termica, inventario.obter(0) );
        assertEquals( energetico, inventario.obter(1) );
        
        assertEquals( 2, resultado.size() );
    }
}