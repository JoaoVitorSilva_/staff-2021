import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class ElfoNoturnoTest {
    
    
    @Test
    public void elfoNoturnoAtiraFlechaGanhaTriploDeXPMasPerde15DeVida(){
        ElfoNoturno elfo = new ElfoNoturno("Legolas");
        Dwarf dwarf = new Dwarf("Zangado");
        elfo.atirarFlecha(dwarf);
        assertEquals(1, elfo.getFlecha().getQuantidade());
        assertEquals(3, elfo.getExperiencia());
        assertEquals(85.0, elfo.getVida());
    }
    
   
}
