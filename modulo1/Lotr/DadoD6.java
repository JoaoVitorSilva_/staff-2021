import java.util.Random;

public class DadoD6 implements SortearNumero {
    
    @Override
    public int sortear() {
        Random random = new Random();
        return random.nextInt(6) + 1;
    }
}
