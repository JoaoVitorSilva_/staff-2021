import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class ElfoTest {
    
    @Test
    public void exemploMudancaValoresItem(){
        Elfo elfo = new Elfo("Legolas");
        Item flecha = elfo.getFlecha();
        flecha.setQuantidade(1000);
        assertEquals( 1000, flecha.getQuantidade() );
    }
    
    @Test
    public void elfoDeveNascerCom2Flechas(){
        Elfo elfo = new Elfo("Legolas");
        assertEquals(2, elfo.getFlecha().getQuantidade() );
    }
    
    @Test 
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP(){
        Elfo elfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Zangado");
        elfo.atirarFlecha(dwarf);
        assertEquals(1, elfo.getFlecha().getQuantidade());
        assertEquals(1, elfo.getExperiencia());
    }
    
    @Test
    public void elfoAtira3FlechasGanhaMenosXP(){
        Elfo elfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Zangado");
        for (int i = 0; i < 3; i++) {
            elfo.atirarFlecha(dwarf);
        }
        assertEquals(2, elfo.getExperiencia());
        assertEquals(0, elfo.getFlecha(). getQuantidade());
    }
}
