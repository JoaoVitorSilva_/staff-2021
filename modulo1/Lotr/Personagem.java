public abstract class Personagem {
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia, qtdExperienciaPorAtaque;
    protected double qtdDano;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.experiencia = 0;
        this.inventario = new Inventario();
        this.qtdExperienciaPorAtaque = 1;
        this.qtdDano = 0.0;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
        this.inventario = new Inventario();
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public void ganharItem( Item item ) {
        this.inventario.adicionar(item);
    }
    
    public void perderItem( Item item ) {
        this.inventario.remover( item );
    }
    
    protected void aumentarXP() {
        this.experiencia += qtdExperienciaPorAtaque;
    }
    
    protected boolean podeTomarDano(){
         return this.vida > 0;
    }
    
    private double defender() {
        return this.vida -= 5 ;
    }
    
    private Status validacaoStatus() {
        return this.vida == 0 ? Status.MORTO : Status.SOFREU_DANO;    
    }
    
    public void tomaDano(){
        if(this.podeTomarDano()){
          this.vida -= this.vida >= this.qtdDano ? this.qtdDano : 0;
          this.status = this.validacaoStatus();
        }     
    }
    
    public abstract String imprimirNomeClasse();
}