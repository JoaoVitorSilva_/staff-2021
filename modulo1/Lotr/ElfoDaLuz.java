import java.util.*;

public class ElfoDaLuz extends Elfo {
    private final double QTD_VIDA_GANHA = 10;
    private int qtdAtaques;
    private final ArrayList<String> DESCRICOES_POSSIVEIS = new ArrayList<>( Arrays.asList( 
    "Espada de Galvorn" ));
    
    {
        this.qtdAtaques = 0;
    }
    
    
    public ElfoDaLuz( String nome ) {
        super(nome);
        super.qtdExperienciaPorAtaque = 1;
        qtdDano = 21;
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_POSSIVEIS.get(0) ));
    }
    
    @Override
    public void perderItem( Item item ) {
        if( item.getDescricao().equals( DESCRICOES_POSSIVEIS.get(0) )){
            this.inventario.remover( item );
        }
    }
    
    private boolean devePerderVida() {
        return qtdAtaques % 2 == 1;
    }
    
    private Item getEspada(){
        return this.getInventario().buscar(DESCRICOES_POSSIVEIS.get(0) );
    }
    
    private void ganharVida() {
        super.vida += QTD_VIDA_GANHA;
    }
    
    public void atacarComESpada( Dwarf dwarf ) {
        Item espada = getEspada();
        if( espada.getQuantidade() > 0 ) {
            qtdAtaques++;
            dwarf.tomaDano();
            if( this.devePerderVida() ) {
                tomaDano();
            } else {
                ganharVida();
            }
        }
    }
}