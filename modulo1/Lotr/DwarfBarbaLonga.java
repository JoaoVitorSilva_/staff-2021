public class DwarfBarbaLonga extends Dwarf {
    private SortearNumero sortearNumero;
    
    public DwarfBarbaLonga( String nome ) {
        super(nome);
        this.sortearNumero = new DadoD6();
    } 
    
    public DwarfBarbaLonga( String nome, SortearNumero sortearnumero ) {
        super(nome);
        this.sortearNumero = sortearnumero;
    }
    
    public void sofrerDano() {
        boolean perdeVida = sortearNumero.sortear() <= 4;
        if( perdeVida ) {
            super.tomaDano();
        }
    }
}
