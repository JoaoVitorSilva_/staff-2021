import java.util.*;
public class ElfoVerde extends Elfo {
    
    private final ArrayList<String> DESCRICOES_POSSIVEIS = new ArrayList<>( Arrays.asList( 
    "Espada de Aço Valiriano", "Arco de Vidro", "Flecha de Vidro"));
    
    
    public ElfoVerde( String nome ){
        super(nome);
        super.qtdExperienciaPorAtaque = 2;
    }
    
    private boolean isDescricaoOK( String descricao ){
        return DESCRICOES_POSSIVEIS.contains( descricao );    
    }
    
    @Override
    public void ganharItem( Item item ) {
        if( this.isDescricaoOK(item.getDescricao()) ) {
            super.inventario.adicionar(item);
        }
    }
    
    //Nao e obrigatorio colocar o @Override
    public void perderItem( Item item ) {
        if( this.isDescricaoOK(item.getDescricao()) ) {
            super.inventario.remover(item);
        }
    }
}

