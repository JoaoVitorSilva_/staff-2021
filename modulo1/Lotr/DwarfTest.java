import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    
    @Test
    public void anaoDeveNascerCom110DeVida() {
        Dwarf dwarf = new Dwarf( "Gimli" );
        assertEquals( 110.0, dwarf.getVida(), 0.00000001 );
        assertEquals( Status.RECEM_CRIADO, dwarf.getStatus() );
    }
    
    @Test
    public void anaoSofreDanoEFicaCom100DeVida() {
        Dwarf dwarf = new Dwarf( "Gimli" );
        dwarf.tomaDano();
        assertEquals( 100.0, dwarf.getVida(), 0.00000001 );
        assertEquals( Status.SOFREU_DANO, dwarf.getStatus() );
    }
    
    @Test
    public void anaoSofreDano12VezesEFicaCom0DeVida() {
        Dwarf dwarf = new Dwarf( "Gimli" );
        for( int i = 0; i < 12; i++ ) {
            dwarf.tomaDano();
        }
        assertEquals( 0.0, dwarf.getVida(), 0.00000001 );
        assertEquals( Status.MORTO, dwarf.getStatus() );
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario() {
        Dwarf dwarf = new Dwarf( "Gimli" );
        Item esperado = new Item( 1, "Escudo" );
        Item resultado = dwarf.getInventario().obter(0);
        assertEquals( esperado, resultado );
    }
    
    @Test
    public void dwarfEquipadoTomaMetadeDoDano() {
        Dwarf dwarf = new Dwarf( "Gimli" );
        dwarf.equiparEDesequiparEscudo();
        dwarf.tomaDano();
        assertEquals( 105.0, dwarf.getVida(), 0.00001 );
    }
    
    @Test
    public void dwarfNaoEquipadoTomaDanoTotal() {
        Dwarf dwarf = new Dwarf( "Gimli" );
        dwarf.tomaDano();
        assertEquals( 100.0, dwarf.getVida(), 0.00001 );
    }
}
