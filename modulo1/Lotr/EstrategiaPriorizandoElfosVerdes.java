import java.util.*;
public class EstrategiaPriorizandoElfosVerdes implements EstrategiaDeAtaque {
    
    private ArrayList<Elfo> ordenacao( ArrayList<Elfo> elfos ) {
        boolean houveTroca = true;
        while( houveTroca ) {
            houveTroca = false;
            
            for( int i = 0; i < elfos.size(); i++ ) {
                Elfo elfoAtual = elfos.get(i);
                Elfo elfoProximo = elfos.get( i + 1 );
                
                boolean precisaTrocar = elfoAtual instanceof ElfoNoturno && elfoProximo instanceof ElfoVerde;
                
                if( precisaTrocar ) {
                    elfos.set( i, elfoProximo );
                    elfos.set( i + 1, elfoAtual);
                    houveTroca = true;
                }
            }
        }
        return elfos;
    }
    
    private ArrayList<Elfo> collections( ArrayList<Elfo> elfos ) {
        Collections.sort( elfos, new ComparadorDeElfos());   
        return elfos;
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque( ArrayList<Elfo> atacantes ) {
        return collections( atacantes );
    }
}